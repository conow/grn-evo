import java.util.*;
import java.util.concurrent.*;
import java.util.zip.*;
import java.util.stream.*;
import java.io.*;
//import java.lang.*;
import java.nio.file.*;
import java.nio.file.StandardCopyOption.*;
import java.math.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;


public class SimStart{
    private static FileOutputStream fos;
    private static GZIPOutputStream gos;
    private static ObjectOutputStream oos;

    static ExecutorService pool;
    static CompletionService<Cell> compS;
    static int poolRemaining;
    static ExecutorService output;

    static int i = 0;
    static boolean done;
    private static Scanner kb = new Scanner(System.in);
    static final boolean verbose = false;
    static final Calendar now = new GregorianCalendar();
    static String baseDumpOut;
    static String baseLineOut;
    static String baseNetOut;
    static boolean baseDump = false;
    static boolean statDump = false;
    static boolean lineDump = false;
    static String sFloc;
    static String faLoc;
    static boolean dumpOut = false;
    static boolean lineOut = false;
    //static boolean sex = true;
    //static boolean recomb = true;
    static boolean setSeed = false;
    static boolean settingsFile = false;
    static boolean genTargetFromCell = false;
    static boolean startFromInd = true;
    static final Random r = new Random();
    static long initSeed = r.nextLong();
    static {
        r.setSeed(initSeed);
    }
    static double ptRate = 0.0001;
    static double rcRate = 0.0001;
    static int maxIter = 50001;
    static int size = 1000;
    static int randEnv = 0;
    static int numRand = 0;
    static int burnIn = 0;
    static boolean cycleEnv = false;
    static double propKill = 1/16.;
    static Path genomeFile = Paths.get("seed0.gen");
    static boolean recMeth = false;
    static boolean newOut = false;
    static int numCores;

    static double killSelect = 10;
    static double repSelect = -10;
    static int fitFunc = 0;
    static boolean restartFromSnapshot = false;
    static Path snapPath;

    static int setReactNum = -1;

    // this is a redundant variable. I'm sorry
    static int iternum = 0;
    static int iterout = 1000;
    static Path signalFile = Paths.get("signal0.1_1");
    static Path tcFn;    
    static boolean netAnalysis = false;
    static boolean crossPops = false;
    static Path pop1;
    static Path pop2;
    static int signalSize = Gene.signalLen;
    static int phenoSize = Gene.phenoLen;
    static int peturbSize = Gene.peturbLen;
    static int internalSize = Gene.internalLen;
    static List<double[]> signal;// = new double[signalSize];
    static BufferedWriter f[] = {new BufferedWriter(new OutputStreamWriter(System.out)), null};

    static String grnFileName = "t3";
    static int internalIters = 5;
    static double internalIntStep = 0.1;
    
    static int fn = -1;
    static boolean useReps = false;
    
    // flags so that we can override settings file flags with flags from the command line
    //
    //
    // ...flags
    // this is getting out of hand. Using some sort of data structure may be a better idea
    // probably should just make a json object that gets populated by parseArgs and then
    // check for the presece of keys
    static boolean doSet = false;
    static boolean ssSet = false;
    static boolean sfSet = false;
    static boolean loSet = false;
    static boolean nmSet = false;
    static boolean niSet = false;
    static boolean mcSet = false;
    static boolean bsSet = false;
    static boolean reSet = false;
    static boolean bpSet = false;
    static boolean pkSet = false;
    static boolean gfSet = false;
    static boolean mrSet = false;
    static boolean rmSet = false;
    static boolean riSet = false;
    static boolean rrSet = false;
    static boolean ntSet = false;
    static boolean ffSet = false;
    static boolean tmSet = false;
    static boolean tlSet = false;
    static boolean tiSet = false;
    static boolean rnSet = false;
    static boolean tfSet = false;
    static boolean rsSet = false;
    static boolean naSet = false;
    static boolean cpSet = false;
    static boolean csSet = false;
    static boolean gsSet = false;
    static boolean faSet = false;
    static boolean ksSet = false;
    static boolean slSet = false;
    static boolean plSet = false;
    static boolean blSet = false;
    static boolean ilSet = false;
    static boolean mmSet = false;
    static boolean scSet = false;
    static boolean maSet = false;
    static boolean enSet = false;
    static boolean fnSet = false;
    static boolean ceSet = false;
    static boolean caSet = false;
    static boolean bfSet = false;
    static boolean siSet = false;
    static boolean noSet = false;
    static boolean tcSet = false;
    static boolean pgSet = false;
    static boolean fpSet = false;
    
    public static void main(String [] args) throws IOException, DeathCountException{
        try{
            numCores = Integer.parseInt(System.getenv("SLURM_CPUS_PER_TASK")) - 1;
        }
        catch(NumberFormatException ex){
            System.err.println("Warning: could not detect number of cores -- numCores set to 4");
            numCores = 4;
        }
        parseArgs(args);

        System.out.println(initSeed);

        if(settingsFile == true){
            readSettings();
        }

        Environment e;
        BufferedReader in = new BufferedReader(new FileReader(signalFile.toString()));
        String s = "";

        if(slSet){
            Gene.setSignalLen(signalSize);
        }

        if(blSet){
            Gene.setPeturbLen(peturbSize);
        }

        signal = new ArrayList<double[]>();
        List<String[]> signalLines = in.lines().map(k->k.trim().split("\\s+")).collect(Collectors.toList());
        for(String[] sl : signalLines){
            double[] sig = new double[sl.length];
            for(int i = 0; i < sl.length; i++){
                sig[i] = Double.parseDouble(sl[i]);
            }
            signal.add(sig);
        }

        if(signal.size() < signalSize){
            System.err.println("Error: signal file specifies fewer signal distributions than required!");
            System.err.println("Signal file length: " + signal.size() + "\nRequired: " + signalSize);
            System.err.println("Eiter use a longer signal file, or adjust the -sl command line parameter!");
            shutDownEverything();
        }

        if(plSet){
            Gene.setPhenoLen(phenoSize);
        }

        if(ilSet){
            Gene.setInternalLen(internalSize);
        }

        pool = Executors.newFixedThreadPool(numCores);
        compS = new ExecutorCompletionService<Cell>(pool);
        output = Executors.newCachedThreadPool();

        if(dumpOut){
            baseDumpOut += makeDirs(baseDumpOut, fn) + "/";
        }
        if(lineOut){
            baseLineOut += makeDirs(baseLineOut, fn) + "/";
            f[1] = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(baseLineOut + "/lineOut.out.gz"), 256000)));
            baseDumpOut = baseLineOut;
        }
        if(netAnalysis){
            baseNetOut += "/nets/";
            baseNetOut += makeDirs(baseNetOut, fn) + "/";
        }
        if(setSeed){
            r.setSeed(initSeed);
        }   

        Long start = System.nanoTime();
        int i = 0;
        Genome fittest = null;
        done = false;

        Genome[] genomes = new Genome[size];

        if(startFromInd){
            Genome test = new Genome(genomeFile, new Random(r.nextLong()), ptRate, rcRate);
            Arrays.fill(genomes, test);
        }
        else{
            genomes = Genome.makePopGenomes(genomeFile, new Random(r.nextLong()), ptRate, rcRate);
        }

        e = initE(genomes, f);

        if(!pgSet)
            e.run(maxIter, numCores, compS);
        else
            e.perturbGenes(numCores, compS);

        f[0].flush();
        f[0].close();
        if(f[1] != null){
            f[1].flush();
            f[1].close();
        }
        output.shutdown();
        pool.shutdown();
        System.out.println((System.nanoTime() - start)/1000000000.);
    }

    private static Environment initE(Genome[] genomes, Writer[] f) throws IOException, DeathCountException{
        Environment e;
        Environment.EnvBuilder eb = new Environment.EnvBuilder(size, ptRate, rcRate, signal,
            new Random(r.nextLong()), genomes, propKill, output,
            iterout, killSelect, repSelect, f
            ).randEnv(randEnv, burnIn, numRand, riSet).cycleEnv(cycleEnv);
        if(tcSet){
            Genome tarGenome = new Genome(tcFn, new Random(r.nextLong()), 0, 0);
            Cell targetCell = new Cell(-1, new Random(r.nextLong()), tarGenome, 0, 0, null, null, 0);
            genTargetFromCell = true;
            eb.targetCell(targetCell);
        }
        if(pgSet){
            eb.setupPerturb(genomeFile);
        }
        if(dumpOut)
            eb.dumpOut(baseDumpOut);
        e = eb.build();
        return e;
    }

    // need to set this up so that it doesn't override command line flags
    private static void readSettings() throws IOException{
        JsonNode sf = Environment.m.readTree(new FileInputStream(sFloc));
        if(!mrSet){ptRate = sf.get("ptRate").asDouble();}
        if(!rrSet){rcRate = sf.get("rcRate").asDouble();}
        // currently no command line flag for indels. Should probably add that...
        if(!niSet){maxIter = sf.get("maxIter").asInt();}
        if(!bsSet){size = sf.get("size").asInt();}
        if(!reSet){randEnv = sf.get("randEnv").asInt();}
        if(!bpSet){burnIn = sf.get("burnIn").asInt();}
        if(!pkSet){propKill = sf.get("propKill").asDouble();}
        if(!rmSet){recMeth = sf.get("recMeth").asBoolean();}

        // fix this later. anything in here means that a previous version
        // of the simulation did not have these items
        try{
            if(!ksSet){killSelect = sf.get("killSelect").asDouble();}
            if(!scSet){repSelect = sf.get("repSelect").asDouble();}
        }
        catch(NullPointerException npe){
            System.err.println(npe.getMessage());
            System.err.println("WARNING: some options are missing from settings file. Using default values");
        }
        // this is kind of jank, but whatever
        if(faSet){
            snapPath = Paths.get(faLoc + "/post/" + (maxIter-1) + ".out.gz");
            if(!Files.exists(snapPath))
                snapPath = Paths.get(faLoc + "/pre/" + (maxIter-1) + ".out.gz");
            maxIter = 50;
        }
    }
    
    private static int makeDirs(String out, int n) throws IOException{
        File outDirBase = new File(out);
        while(!outDirBase.exists()){
            boolean made = outDirBase.mkdirs();
        }
        int numInFolder = outDirBase.listFiles().length;
        if(n > -1)
            numInFolder = n;
        out += numInFolder + "/";
        File outDir = new File(out);
        if(outDir.exists()){
            System.err.println("*** "+out);
            for(File s : outDirBase.listFiles()){
                System.err.println(s);
            }
            System.err.println("Trying to use a directory that already exists! Aborting to avoid overwriting previous results. " +
                               "If you're running jobs in parallel using the same parameters, consider using the -fn # option to ensure each independent run gets its own folder.");
            System.exit(1);
        }
        outDir.mkdirs();

        FileWriter fw = new FileWriter(out + "settings");
        TreeMap<String, Object> settingsFile = new TreeMap<>();
        settingsFile.put("ptRate", ptRate);
        settingsFile.put("rcRate", rcRate);
        settingsFile.put("initSeed", initSeed);
        settingsFile.put("size", size);
        settingsFile.put("randEnv", randEnv);
        settingsFile.put("burnIn", burnIn);
        settingsFile.put("propKill", propKill);
        settingsFile.put("maxIter", maxIter);
        if(!restartFromSnapshot)
            settingsFile.put("genomeFile", genomeFile.toString());
        settingsFile.put("recMeth", recMeth);
        settingsFile.put("numCores", numCores);
        //settingsFile.put("recomb", recomb);
        settingsFile.put("killSelect", killSelect);
        settingsFile.put("repSelect", repSelect);
        settingsFile.put("signal", signalFile.toString());
        settingsFile.put("signalSize", signalSize);
        settingsFile.put("phenoSize", phenoSize);
        settingsFile.put("peturbSize", peturbSize);
        // using the json mapper defined in environment to write the file...
        // this seems kind of incorrect but whatever
        fw.write(Environment.m.writeValueAsString(settingsFile));

        fw.flush();
        fw.close();
        f[0].flush();
        f[0].close();
        f[0] = new BufferedWriter(new FileWriter(out+"stdOut"));
        return numInFolder;
    }
    // if an exception is thrown anywhere, we want to shut down all the threads
    // because the purpose of exceptions is to give a chance for an application
    // to shut down gracefully, not to act as an "if/then" sort of thing
    // so when we catch one, we call this
    static void shutDownEverything(){
        System.out.println("Shutting down thread pools...");
        output.shutdown();
        pool.shutdown();
    }

    // At this point, it may be better to have a separate file with all the flags and options
    // but this works for now
    private static void parseArgs(String[] args){
        for(int i = 0; i < args.length; i++){
            switch(args[i]){
                case "-do": // do for dump out. Additionally requires a path to write files. Outputs "full" information every 1000 iterations
                doSet = true;
                dumpOut = true;
                newOut = true;
                if(i + 1 < args.length){
                    i++;
                    baseDumpOut = args[i];
                }
                break;
                case "-ss": // ss for set seed. Allows (in theory) replicate runs to gather more data. This hasn't been tested in a while though, and with parallel processing who knows
                ssSet = true;
                setSeed = true;
                if(i + 1 < args.length){
                    i++;
                    initSeed = Long.parseLong(args[i]);
                }
                break;
                case "-sf": // sf for settings file. Requires a path, and then settings from it. These settings are overridden by additional explicit command line flags
                sfSet = true;
                settingsFile = true;
                if(i + 1 < args.length){
                    i++;
                    sFloc = args[i];
                }
                else{
                    System.err.println("Need to specify file path for settings file!");
                    System.exit(1);
                }
                break;
                case "-lo": // lo for line out. Additionally requires a path to write files. Essentially dump out but every iteration. Also outputs lineage information
                loSet = true;
                lineOut = true;
                if(i + 1 < args.length){
                    i++;
                    baseLineOut = args[i];
                }
                break;
                case "-ni": // ni for number of iterations. Requires an integer.
                niSet = true;
                if(i + 1 < args.length){
                    i++;
                    maxIter = Integer.parseInt(args[i]);
                }
                break;
                case "-bs": // bs for boardsize.
                bsSet = true;
                if(i + 1 < args.length){
                    i++;
                    size = Integer.parseInt(args[i]);
                }
                break;
                case "-re": // re for randomized environment. Takes an integer which specifies the period of randomization
                reSet = true;
                try{
                    if(i + 2 < args.length){
                        i++;
                        randEnv = Integer.parseInt(args[i]);            
                        i++;
                        numRand = Integer.parseInt(args[i]);
                    }
                }
                catch(NumberFormatException nfe){
                    System.err.println("-re flag requires two values. The first is the frequency of change, and the second is the maximum amount of change that can occur in a single step\n");
                    shutDownEverything();
                    System.exit(1);
                }
                break;
                case "-ri": // ri for randomized (per) individual. If this is set, the environment is randomized independently for each individual
                riSet = true;
                break;
                case "-bp": // bp for burn period for random environments. For some parameter sets, the initial population may require some time to adapt before it can survive randomization.
                bpSet = true;
                if(i + 1 < args.length){
                    i++;
                    burnIn = Integer.parseInt(args[i]);
                }
                break;
                case "-pk": // pk for proportion to kill per iteration. Takes a float in the range [0, 1]
                pkSet = true;
                if(i + 1 < args.length){
                    i++;
                    propKill = Double.parseDouble(args[i]);
                }
                break;
                case "-gf": // gf for genome file. Allows you to specify where it's reading the initial genome from
                gfSet = true;
                if(i + 1 < args.length){
                    i++;
                    genomeFile = Paths.get(args[i]);
                }
                break;
                case "-si": // for signal file
                siSet = true;
                if(i+1 < args.length){
                    i++;
                    signalFile = Paths.get(args[i]);
                }
                break;
                case "-mr": // mr for mutation rate per base. Takes as input a float
                mrSet = true;
                if(i + 1 < args.length){
                    i++;
                    ptRate = Double.parseDouble(args[i]);
                }
                break;
                case "-rm": // rm for recombination method. Defaults to prob per base, using this flag makes it per chrom
                rmSet = true;
                recMeth = true;
                break;
                case "-rr": // rr for recombination rate per chromosome by default, or per base if rm flag is used. Takes as input a flot
                rrSet = true;
                if(i + 1 < args.length){
                    i++;
                    rcRate = Double.parseDouble(args[i]);           
                }
                break;
                case "-nt": // nt for number of threads. Use this to specify number of threads to use for running the simulation
                ntSet = true;
                if(i +1 < args.length){
                    i++;
                    numCores = Integer.parseInt(args[i]);
                }
                break;
                case "-ks": // ks for kill selection coefficient
                ksSet = true;
                if(i + 1 < args.length){
                    i++;
                    killSelect = Double.parseDouble(args[i]);
                }
                break;
                case "-sc": // sc for selection coefficient (for reproduction)
                scSet = true;
                if(i + 1 < args.length){
                    i++;
                    repSelect = Double.parseDouble(args[i]);
                }
                break;
                case "-fn": // for folder number (if queueing a lot of simulations with the same parameters, they might otherwise try to write to the same folders)
                fnSet = true;
                if(i + 1 < args.length){
                    i++;
                    fn = Integer.parseInt(args[i]);
                }
                break;
                case "-ce": // for cycle environment
                ceSet = true;
                cycleEnv = true;
                break;
                case "-no": // for new (std) output. stdOut will then have a header and be comma separated as well as output more statistics.
                noSet = true;
                newOut = true;
                break;
                case "-tc": // for target cell. If this is specified, target phenotypes are obtained by keeping a pre-defined cell alive the entire simulation and using it's phenotypic output as the target against which the fitness of individuals in the evolving population are compared
                tcSet = true;
                if(i+1 < args.length){
                    i++;
                    tcFn = Paths.get(args[i]);
                }
                break;
                case "-pg": // for perterb genes. This should only be called when loading a previously evolved cell to see what the effect of doing pairwise perturbations on its genome is
                pgSet = true;
                break;
                case "-tf": // for target file
                tfSet = true;
                if(i+1 < args.length){
                    i++;
                    grnFileName = Paths.get(args[i]).toString();
                }
                break;
                case "-sl": // for signal length
                slSet = true;
                if(i + 1 < args.length){
                    i++;
                    signalSize = Integer.parseInt(args[i]);
                }
                break;
                case "-pl": // for pheno length
                plSet = true;
                if(i + 1 < args.length){
                    i++;
                    phenoSize = Integer.parseInt(args[i]);
                }
                break;
                case "-bl": // for perturb length, only makes sense to use in conjunction with -re
                blSet = true;
                if(i + 1 < args.length){
                    i++;
                    peturbSize = Integer.parseInt(args[i]);
                }
                break;
                case "-il": // for internal length
                ilSet = true;
                if(i + 1 < args.length){
                    i++;
                    internalSize = Integer.parseInt(args[i]);
                }
                break;
                case "-mm": // for mutable mutation
                mmSet = true;
                Cell.mutableRates = true;
                break;
                case "-fp": // for from population
                fpSet = true;
                startFromInd = false;
                break;
                default:
                System.err.println("Unknown argument(s):\t" + args[i] + "\t Aborting.");
                System.exit(1);
            }
        }
    }
}
