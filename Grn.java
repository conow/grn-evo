import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.nio.file.*;
import java.util.stream.*;
import java.lang.*;
import org.apache.commons.math3.ode.*;
import org.apache.commons.math3.ode.sampling.*;
import org.apache.commons.math3.ode.nonstiff.*;
import org.apache.commons.math3.ode.events.*;
import org.apache.commons.math3.linear.*;
import org.apache.commons.jexl3.*;


public abstract class Grn{
    double[][] expressTraj = new double[0][0];
    double[] pairDist = new double[0];
    static boolean nonneg = true;
    
    abstract void makeExpressTraj(int numWhole, double stepSize, double[] signal);
    abstract BlockRealMatrix getPhenoTraj();
    abstract public String toString();
    abstract Grn copyGrn();

    public static void main(String[] args) throws IOException{
        /*
          double[][] t = new double[5][5];
          t[0] = new double[]{ -1, 2, 0,-2, 0};
          t[1] = new double[]{ -1,-1, 0,-0, 0};
          t[2] = new double[]{  1, 0,-1, 0, 0};
          t[3] = new double[]{  0, 0, 0, 0, 0};
          t[4] = new double[]{  0,-1, 1, 1, 0};
        */
        String fn = "test.sin.csv";
        int numSteps = SimStart.internalIters;
        double stepSize = SimStart.internalIntStep;
        if(args.length > 0){
            fn = args[0];
        }
        if(args.length > 1){
            numSteps = Integer.parseInt(args[1]);
        }
        if(args.length > 2){
            nonneg = args[2].toLowerCase().equals("t");
        }
        System.err.println(fn);
        Grn test = Grn.grnFromFile(fn);
        //test.setValues(new double[]{0, 0, 0, 0, 0});
        //System.out.println(prettyString(test.grn.getData()));
        //System.out.println(Arrays.toString(test.values));
        //test.makeExpressTraj(100);
        //test.expressTraj = new double[20][5];
        //test.expressTraj[0] = new double[]{0, 0, 0, 0, 10};
        //double[] temp = new double[]{0, 0, 0, 0, 10};
        //double[] result = new double[5];
        //test.integ.addStepHandler(test.stepHandler);
        test.makeExpressTraj(numSteps, stepSize, new double[]{0});
        //EigenDecomposition testEigen = new EigenDecomposition(((MatrixGrn)test).compactGrn);
        System.out.println(prettyString(test.expressTraj));   
        //System.out.println(Arrays.toString(testEigen.getImagEigenvalues()));
        //System.out.println(Arrays.toString(testEigen.getRealEigenvalues()));
    }


    
    static Grn grnFromFile(String fn) throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(fn));
        String type = in.readLine().trim().toLowerCase();
        Grn result = null;
        switch(type){
            case "## matrix":
            List<String[]> temp = in.lines().map(s->s.trim().split("\\s+")).collect(Collectors.toList());
            result = new MatrixGrn(loadMatFile(temp));
            break;
            case "## equation":
            int numEnv = Integer.parseInt(in.readLine().substring(2).trim());
            in.close();
            result = new EquationGrn(fn);
            break;
            default:
            System.err.println("ERROR! Grn file of unknown format. Header is neither \"## matrix\" nor \"## equation\":\n" + type + "\nis the first line in " + fn);
            break;
        }
        return result;
    }

    static double[][] loadMatFile(List<String[]> temp) throws IOException{
        int n = temp.size();
        int m = temp.get(0).length;      
        double[][] result = new double[n][m];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                result[i][j] = Double.parseDouble(temp.get(i)[j]);
            }
        }
        //System.err.println(n+"\t"+result.length+"\t"+result[0].length);
        return result;
    }

    static double[] pairwiseDistance(BlockRealMatrix p1, BlockRealMatrix p2){
        double[] result = new double[p1.getColumnDimension()];
        for(int i = 0; i < p1.getColumnDimension(); i++){
            try{
                result[i] = p1.getColumnVector(i).getDistance(p2.getColumnVector(i));
            }
            catch(Exception e){
                System.err.println(String.format("%d\t%d",p1.getRowDimension(),p2.getRowDimension()));
                throw(e);
            }
        }
        return result;
    }

    static double manhattanDistance(Grn a, Grn b){
        BlockRealMatrix x = a.getPhenoTraj();
        BlockRealMatrix y = b.getPhenoTraj();
        double[] pairDist = pairwiseDistance(x, y);
        double result = 0;
        for(double d : pairDist){
            result += d;
        }
        // casting block matrices to strings is EXPENSIVE. Don't uncomment the below unless you need it for debugging
        //String outString = "a\t"+x+"\nb\t"+y+"\nresult\t"+result;
        //System.out.println(outString);
        return result;
    }

    static String prettyString(double[][] a){
        return "-\n"+Arrays.stream(a).map(s -> Arrays.toString(s)).collect(Collectors.joining("\n"))+"\n-";
    }
    public String exprString(){
        return Arrays.stream(expressTraj).map(s -> Arrays.toString(s)).collect(Collectors.joining("\n"));
    }

    void setPairDist(Grn other){
        BlockRealMatrix thisP = this.getPhenoTraj();
        BlockRealMatrix otherP = other.getPhenoTraj();
        pairDist = pairwiseDistance(thisP, otherP);
    }
    
    double[][] deepClone(double[][] orig){
        double[][] result = new double[orig.length][orig[0].length];
        for(int i = 0; i < orig.length; i++){
            result[i] = orig[i].clone();
        }
        return result;
    }
    

}

class EquationGrn extends Grn{
    JexlEngine jexl;
    JexlScript grn;
    JexlContext context;
    double[] values = new double[0];
    final int exprLen;
    String scriptString;
    int eqHash;
    static ConcurrentMap<Integer, ConcurrentMap<Integer, double[][]>> seenResults = new ConcurrentHashMap<>();
    public EquationGrn(String fn) throws IOException{
        Map<String, Object> ns = new HashMap<>();
        ns.put("Math", Math.class);
        jexl = new JexlBuilder().cache(512).strict(true).silent(false).namespaces(ns).create();
        grn = jexl.createScript(new File(fn));
        scriptString = Files.lines(Paths.get(fn)).collect(Collectors.joining("\n"));
        eqHash = scriptString.hashCode();
        context = new MapContext();
        context.set("e", new double[Gene.signalLen]);
        context.set("t", 0);
        context.set("i", 0);
        double[] test;
        try{
            test = (double[])grn.execute(context);
        }
        catch(JexlException e){
            System.err.println("ERROR: Are you sure that Gene.signalLen is large enough for the given jexl script from " + fn + "?");
            throw(e);
        }
        exprLen = test.length;
        /**/

        //double[] a = new double[]{10, 2, 3};
        //context.set("e", a);
        //int v = (int) e.execute(context);
        //double i = 0;
        //for(i = 0; i < 10; i += 0.25){
        //context.set("t", i);
        //System.out.println(Arrays.toString((double[])grn.execute(context)));
        //}
        //exprLen = 0;
    }

    EquationGrn(EquationGrn orig){
        this.jexl = orig.jexl;
        this.grn = orig.grn;
        this.context = new MapContext();
        this.scriptString = orig.scriptString;
        this.exprLen = orig.exprLen;
    }
    
    public String toString(){
        return scriptString;
    }

    EquationGrn copyGrn(){
        return new EquationGrn(this);
    }

    void makeExpressTraj(int numWhole, double stepSize, double[] signal){
        if(seenResults.containsKey(eqHash)){
            if(seenResults.get(eqHash).containsKey(Arrays.hashCode(signal))){
                expressTraj = deepClone(seenResults.get(eqHash).get(Arrays.hashCode(signal)));
                return;
            }
        }
        seenResults.putIfAbsent(eqHash, new ConcurrentHashMap<Integer, double[][]>());
        if(expressTraj.length != Math.floor(numWhole/stepSize)){
            expressTraj = new double[(int)Math.floor(numWhole/stepSize)+1][exprLen];
        }
        context.set("e", signal);
        context.set("i", SimStart.iternum);
        for(int i = 0; i < Math.floor(numWhole/stepSize+1); i++){
            context.set("t", i*stepSize);       
            expressTraj[i] = (double[]) grn.execute(context);
        }
        seenResults.get(eqHash).put(Arrays.hashCode(signal), deepClone(expressTraj));
        //System.err.println(Arrays.deepToString(expressTraj));
    }

    BlockRealMatrix getPhenoTraj(){
        return new BlockRealMatrix(expressTraj).getSubMatrix(0, expressTraj.length-1, 0, Math.min(exprLen, Gene.phenoLen-1));
    }

    
}

class MatrixGrn extends Grn{
    static ConcurrentMap<Integer, ConcurrentMap<Integer, double[][]>> seenResults = new ConcurrentHashMap<>();
    RealMatrix grn;
    RealMatrix compactGrn;
    int[] presentMap;
    int grnHash;
    int n;
    double[] values = new double[0];
    double[] initialValues = new double[0];
    double rkiStep = SimStart.internalIntStep;
    double[] yn;
    // This will work with any integration method that satisfies the FirstOrderIntegrator API given by
    // the apache commons math package. Several are already implemented in that package, and we use the
    // midpoint method (i.e. RK2) as a reasonable compromise between accuracy and computational speed.
    // More accurate methods may be used if one wishes with no downside other than increased run time;
    // RK4 and an instance of the Adams Bashforth adaptive step integrators are commented out below
    FirstOrderIntegrator integ;
    FirstOrderDifferentialEquations equations = new FirstOrderDifferentialEquations(){
        public int getDimension(){
            return n;
        }
        public void computeDerivatives(double t, double[] y, double[] yDot){
            // In order to not let derivatives be calculated for negative expression values
            // we set any values that were negative in the last step to zero
            double[] mody = new double[y.length];
            for(int i = 0; i < y.length; i++){
                if(y[i] < 0 && nonneg){
                    mody[i] = 0;
                //System.err.println("does this happen?");
                }
                else
                    mody[i] = y[i];
            }
            System.arraycopy(grn.operate(mody), 0, yDot, 0, yDot.length);
            // After calculating the derivatives, if any of them would cause a value to become
            // negative, we want to ensure that it does not
            for(int i = 0; i < yDot.length; i++){
                /*
                  if(i == 0)
                  System.err.print(yn[i] + "\t" + y[i] + "\t" + yDot[i] + "\t");
                */
            double modDiv = 1;
                if(yDot[i] < -yn[i]/(rkiStep) && nonneg){
                    yDot[i] = -yn[i]/(rkiStep);
                }
                /*
                  if(i == 0)
                  System.err.println(mody[i] + "\t" + yDot[i]);
                  /*
                  if(y[i] < 0){
                  yDot[i] = -y[i];
                  }
                */
          }

        }
    };
    
    StepHandler stepHandler = new StepHandler(){
        public void init(double t0, double[] y0, double t){
            yn = y0.clone();
        }
        public void handleStep(StepInterpolator interpolator, boolean isLast){
            int i = (int)Math.round(interpolator.getCurrentTime()/rkiStep);
            double[] y = interpolator.getInterpolatedState();
            /*
              for(int j = 0; j < y.length; j++){
              if(y[j] < 0)
              y[j] = 0;
              }
            */
            //System.err.println(Arrays.toString(y)+"\n"+Arrays.toString(interpolator.getInterpolatedDerivatives()));          
            expressTraj[i] = y.clone();
            yn = expressTraj[i];
        }
    };

    public MatrixGrn(){
        n = Gene.internalLen+Gene.phenoLen+Gene.signalLen;;
        grn = new BlockRealMatrix(n,n);
        //grn = new OpenMapRealMatrix(n,n);
        compressGrn();
        grnHash = grn.hashCode();
        values = new double[n];
        initialValues = new double[n];
        integ = initIntegrator(rkiStep, stepHandler);
    }

    public MatrixGrn(MatrixGrn orig){
        this.n = orig.n;
        this.grn = orig.grn.copy();
        try{
            this.compactGrn = orig.compactGrn.copy();
        }
        catch(Exception e){
            System.err.println(orig.grn);
            throw(e);
        }
        this.presentMap = orig.presentMap.clone();
        this.grnHash = this.grn.hashCode();
        this.values = new double[n];
        this.initialValues = orig.initialValues.clone();
        this.rkiStep = orig.rkiStep;
        this.integ = initIntegrator(rkiStep, stepHandler);
    }

    MatrixGrn copyGrn(){
        return new MatrixGrn(this);
    }


    //FirstOrderIntegrator integ = new ClassicalRungeKuttaIntegrator(rkiStep);
    //FirstOrderIntegrator integ = new AdamsBashforthIntegrator(4, 0.02, 1, 0.1, 0.1);
    static FirstOrderIntegrator initIntegrator(double stepSize, StepHandler stepHandler){
        FirstOrderIntegrator result = new MidpointIntegrator(stepSize);
        //FirstOrderIntegrator result = new ClassicalRungeKuttaIntegrator(stepSize);
        result.addStepHandler(stepHandler);
        return result;
    }

    public void init(List<Gene> genes){
        double[][] temp = new double[n][n];
        initialValues = new double[n];
        for(Gene g : genes){
            if(g.type < 4){
                for(int col : g.bindingSite){
                    if(col < 0)
                        col = n + col;
                    for(int row : g.targets){
                        if(row < 0)
                            row = n + row;
                        try{
                            temp[row][col] += g.regulate;
                        }
                        catch(Exception ex){
                            System.err.println("row\t"+row);
                            System.err.println("col\t"+col);
                            System.err.println("n\t"+n);
                            System.err.println("target\t"+g.target);
                            System.err.println("specificity\t"+g.spec);
                            System.err.println("binding sites\t"+Arrays.toString(g.bindingSite));
                            System.err.println("valid targets\t"+Arrays.toString(g.targets));
                            throw(ex);
                        }
                    }
                }
            }
            else{
                initialValues[g.target] += g.regulate;
            }
        }
        for(int i = 0; i < n; i++){
            if(initialValues[i] < 0){
                initialValues[i] = 0;
            }
        }
        grn = new BlockRealMatrix(temp);
        compressGrn();
        //grn = omrMat(temp);
        grnHash = grn.hashCode();
        values = new double[n];
    }

    public MatrixGrn(double[][] mat){
        double[][] interactions = new double[mat.length-1][mat[0].length];
        System.arraycopy(mat, 0, interactions, 0, interactions.length);
        grn = new BlockRealMatrix(interactions);    
        n = Math.min(grn.getRowDimension(), grn.getColumnDimension());
        if(grn.getRowDimension() != grn.getColumnDimension()){
            System.err.println(String.format("Warning number of rows != number of columns in input file: %d, %d\nSetting n = %d",
                grn.getRowDimension(),
                grn.getColumnDimension(),
                n));
        }
        //System.err.println(grn.getRowDimension() + "\t" + grn.getColumnDimension() + "\t" + prettyString(grn.getData()) + "\t" + n);
        //grn = new BlockRealMatrix(n, n, BlockRealMatrix.toBlocksLayout(grn.getData()), true);
        //grn = omrMat(mat);
        grnHash = grn.hashCode();
        compressGrn();
        values = new double[n];
        initialValues = new double[mat[0].length];
        System.arraycopy(mat[mat.length-1], 0, initialValues, 0, initialValues.length);
        integ = initIntegrator(rkiStep, stepHandler);
    }

    static OpenMapRealMatrix omrMat(double[][] mat){
        OpenMapRealMatrix result = new OpenMapRealMatrix(mat.length, mat[0].length);
        for(int i = 0; i < mat.length; i++){
            for(int j = 0; j < mat[0].length; j++)
                if(mat[i][j] != 0)
                    result.setEntry(i, j, mat[i][j]);
            }
            return result;
        }

        void compressGrn(){
            if(true && false){
                compactGrn = new BlockRealMatrix(1,1);
                presentMap = new int[0];
                return;
            }
            List<Integer> presentTemp = new ArrayList<>();
            int n = grn.getRowDimension();
            for(int i = 0; i < n; i++){
                boolean found = false;
                for(double v : grn.getRow(i)){
                    if(v != 0){
                        presentTemp.add(i);
                        found = true;
                        break;
                    }
                }
                if(found)
                    continue;
                for(double v : grn.getColumn(i)){
                    if(v != 0){
                        presentTemp.add(i);
                        break;
                    }
                }
            }
            presentMap = presentTemp.stream().mapToInt(i->i).toArray();
            int nn = presentMap.length;
            if(nn == 0){
                compactGrn = grn;
                presentMap = IntStream.range(0,n).toArray();
                return;
            }
            double[][] compactArray = new double[nn][nn];
            double[][] origArray = grn.getData();
            for(int i = 0; i < nn; i++){
                for(int j = 0; j < nn; j++){
                    compactArray[i][j] = origArray[presentMap[i]][presentMap[j]];
                }
            }
            compactGrn = new BlockRealMatrix(compactArray);
        }

        double[][] getDoubleArray(){
            return grn.getData();
        }

        public String toString(){
            String result = "## matrix\n";
            for(double[] row : grn.getData()){
                result += Arrays.stream(row).mapToObj(s -> s+"").collect(Collectors.joining("\t"))+"\n";
            }
            result += Arrays.stream(initialValues).mapToObj(s->s+"").collect(Collectors.joining("\t"))+"\n";
            return result;
        }    
        
        void setValues(double[] signal){
            values = initialValues.clone(); 
            for(int i = values.length-signal.length; i < values.length; i++){
                try{
                    values[i] += signal[i-values.length+signal.length];
                }
                catch(Exception e){
                    System.out.println(i+"\t"+values.length+"\t"+signal.length);
                    throw(e);
                }
            }
        }
        
        BlockRealMatrix getPhenoTraj(){
            int start = expressTraj[0].length-Gene.phenoLen-Gene.signalLen;
            return new BlockRealMatrix(expressTraj).getSubMatrix(0, 
                expressTraj.length-1, 
                expressTraj[0].length-Gene.phenoLen-Gene.signalLen,
                expressTraj[0].length-Gene.signalLen-1);
        }

        void makeExpressTraj(int numSteps, double stepSize, double[] signal){
            setValues(signal);
            if(stepSize != rkiStep){
                rkiStep = stepSize;
                integ = initIntegrator(rkiStep, stepHandler);
            }
            if(rkiStep != SimStart.internalIntStep){
                System.err.println("Warning: MatrixGrn makeExpressTraj being called with a step size (" + rkiStep + ") that differs from SimStart.internalIntStep (" + SimStart.internalIntStep + "). Make sure this is intentional");
            }
            //System.err.println(Arrays.toString(values));
            if(seenResults.containsKey(grnHash)){
                if(seenResults.get(grnHash).containsKey(Arrays.hashCode(values))){
                    expressTraj = deepClone(seenResults.get(grnHash).get(Arrays.hashCode(values)));
                    return;
                }
            }
            seenResults.putIfAbsent(grnHash, new ConcurrentHashMap<Integer, double[][]>());
            
            if(numSteps != expressTraj.length)
                expressTraj = new double[(int)(numSteps/rkiStep)+1][n];
            double[] result = new double[n];
            expressTraj[0] = values.clone();
            integ.integrate(equations, 0, values, numSteps, result);
            seenResults.get(grnHash).put(Arrays.hashCode(values), deepClone(expressTraj));
        }
    }
