import java.util.*;
import java.math.*;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.nio.channels.FileChannel;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.stream.IntStream;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import java.util.zip.*;
import java.util.stream.*;

class Environment{
    Cell[] board;
    Cell[] nextBoard;
    // Each cell gets a new Random variable when they change state.
    // With live cells unable to move, it'd make more sense to just
    // have each location have its own random variable that is
    // initialized once, but once/if we allow cells to move,
    // it'll be simpler to have each cell carry it's own Random
    // with it. And this is all so that we can reproduce a simulation
    // if we need to.
    Random[] boardRands;
    long[] boardSeeds; // store seeds rather than randoms, that way multiple cells can mutate

    TreeMap<Double, Cell> breedTree = new TreeMap<Double, Cell>();

    int size;
    ConcurrentSkipListSet<Cell> liveCells = new ConcurrentSkipListSet<>(Cell.byFit);
    NavigableSet<Cell> viableCells = new TreeSet<>(Cell.byFit);
    List<Cell> viableCellsList = new ArrayList<>();
    HashSet<Cell> deadCells = new HashSet<>();
    TreeMap<String, String> summaryMap = new TreeMap<>();
        
    int viableCount = 0;
    ConcurrentLinkedQueue<Cell> deadQueue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<Cell> viableQueue = new ConcurrentLinkedQueue<>();

    int mixEnv;
    int mixNum;
    int maxLen;
    int maxSigVariation = 0;
    int killCount;
    double killGoal;
    int fittestA;
    double fittestM;
    double fittestR;
    int worstAge;
    int fittestNC;
    Cell fitInd;
    int numGen;
    int maxGene;
    int maxUsed;
    int maxUnique;
    int maxAge;
    int minAge;
    int mostOff;
    int mostAtt;
    int mateCount;
    String mostOffd;
    String mostAttd;
    double prevSum = 1;
    double maxFit;
    double maxNormFit;
    double minFit;
    double minNormFit;
    double medFit;
    double medNormFit;
    double sumFit;
    double sumNormFit;
    double sumPhen;
    int maxOff;
    int fittestNumImp;
    int sumAge;
    double sumSize;
    int sumNC;
    int burnIn;
    double propKill;
    Writer[] f;
    static ObjectMapper m = new ObjectMapper();
    int doaCount = 0;
    int iterout;

    Cell targetCell;
    static Grn targetGrn;
    
    Random rand;
    boolean netAnalysis = false;
    boolean dumpOut = false;
    boolean fullRand = true;
    boolean indRand = false;
    String baseDumpOut;
    String baseNetOut;
    double ptRate;
    double rcRate;
    double killSelect;
    double repSelect;
    boolean cycleEnv;
    List<double[]> signal;
    List<Integer> signalInds;
    List<int[]> signalPointers;
    List<double[]> signals;
    //int[] signalPointer;
    Integer iternum = 0;
    ExecutorService output;
    int maxIter = 0;
        
    Environment(EnvBuilder builder) throws IOException{
        this.size = builder.size;
        this.signal = builder.signal;
        // seems silly to not have an option for IntStreams to be turned into a list more concisely, but w/e
        signalInds = IntStream.range(0, Math.min(signal.size(), Gene.signalLen+Gene.peturbLen)).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        //signalPointer = new int[signal.size()];
        this.mixEnv = builder.mixEnv;
        this.mixNum = builder.mixNum;
        this.burnIn = builder.burnIn;
        this.propKill = builder.propKill;
        this.ptRate = builder.ptRate;
        this.rcRate = builder.rcRate;
        this.netAnalysis = builder.netAnalysis;
        this.dumpOut = builder.dumpOut;
        this.baseDumpOut = builder.baseDumpOut;
        this.baseNetOut = builder.baseNetOut;
        this.output = builder.output;
        this.iterout = builder.iterout;
        this.killSelect = builder.killSelect;
        this.repSelect = builder.repSelect;
        this.rand = builder.rand;
        this.cycleEnv = builder.cycleEnv;
        this.targetCell = builder.targetCell;
        this.fullRand = builder.fullRand;
        this.indRand = builder.indRand;
        this.f = builder.f;
        board = new Cell[size];
        nextBoard = new Cell[size];
        boardRands = new Random[size];
        killGoal = Math.ceil(propKill*size);
        genTarget();
        Genome[] genomes = builder.genomes;
        signalPointers = new ArrayList<>();
        for(int i = 0; i < size; i++){
            signalPointers.add(new int[Gene.signalLen+Gene.peturbLen]);
            boardRands[i] = new Random(rand.nextLong());
            int loc = i;
            board[i] = new Cell(loc, boardRands[i], genomes[i], ptRate, rcRate, breedTree, this, iternum);
            board[i].regulation = new HashMap<>(builder.regulation);
            liveCells.add(board[i]);
        }
        for(double[] sig : signal){
            maxSigVariation = Math.max(sig.length, maxSigVariation);
        }
        if(SimStart.newOut){
            summaryMap.put("iter", "");
            summaryMap.put("maxChrLen","");
            summaryMap.put("viableCount", "");
            summaryMap.put("ageOfMostFit", "");
            summaryMap.put("ageOfLeastFit", "");
            summaryMap.put("maxAge","");
            summaryMap.put("avgAge", "");
            summaryMap.put("doaCount", "");
            summaryMap.put("killCount", "");
            summaryMap.put("maxFit","");
            summaryMap.put("minFit","");
            summaryMap.put("medFit","");
            summaryMap.put("avgFit", "");
            summaryMap.put("minNormFit", "");
            summaryMap.put("maxNormFit", "");
            summaryMap.put("medNormFit", "");
            summaryMap.put("avgNormFit", "");
            summaryMap.put("fittestMutRate", "");
            summaryMap.put("fittestRecRate", "");
            summaryMap.put("mostOffspring", "");
            summaryMap.put("mostMateAttmpt", "");
            summaryMap.put("mateSuccess", "");
        }
        updatePopSignals();
        m.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    public static class EnvBuilder{
        private int size;
        private double ptRate;
        private double rcRate;
        private List<double[]> signal;
        private Random rand;
        private Genome[] genomes;
        private double propKill;
        private ExecutorService output;
        private int iterout;
        private double killSelect;
        private double repSelect;
        private boolean cycleEnv = false;
        private boolean indRand = false;
        private boolean netAnalysis = false;
        private boolean dumpOut = false;
        private int mixEnv = 0;
        private int mixNum = 0;
        private boolean fullRand = true;
        private int burnIn = -1;
        private String baseDumpOut = "./";
        private String baseNetOut = "./";
        private Writer[] f;
        private Cell targetCell = Cell.dummyCell();

        private Map<Integer, Double> regulation = new HashMap<>();
        
        public EnvBuilder(int size, double ptRate, double rcRate, List<double[]> signal,
                          Random rand, Genome[] genomes,
                          double propKill, ExecutorService output, int iterout, double killSelect,
                          double repSelect, Writer[] f){
            this.size = size;
            this.ptRate = ptRate;
            this.rcRate = rcRate;
            this.signal = signal;
            this.rand = rand;
            this.genomes = genomes;
            this.propKill = propKill;
            this.output = output;
            this.iterout = iterout;
            this.killSelect = killSelect;
            this.repSelect = repSelect;
            this.f = f;
        }

        public EnvBuilder netAnalysis(String baseNetOut){
            netAnalysis = true;
            this.baseNetOut = baseNetOut;
            return this;
        }

        public EnvBuilder dumpOut(String baseDumpOut){
            dumpOut = true;
            this.baseDumpOut = baseDumpOut;
            return this;
        }
        
        public EnvBuilder randEnv(int frequency, int burnp, int howMany, boolean indr){
            mixEnv = Math.abs(frequency);
            fullRand = frequency >= 0;
            mixNum = howMany;
            burnIn = burnp;
            indRand = indr;
            return this;
        }

        public EnvBuilder cycleEnv(boolean flag){
            cycleEnv = flag;
            return this;
        }

        public EnvBuilder targetCell(Cell targetCell){
            this.targetCell = targetCell;
            return this;
        }

        public EnvBuilder setupPerturb(Path popFile){
            JsonNode pop = loadPop(popFile);
            regulation = setFittestRegs(pop);
            return this;
        }
        
        public Environment build() throws IOException{
            return new Environment(this);
        }

    }

    private void genTarget() throws IOException{
        // These should probably be re-written in a more
        // data-driven fashion (i.e. make the following methods
        // static and take as input whatever relevant data they need
        // and then return an array)
        if(!SimStart.genTargetFromCell){
            genTargetViaMatrix();
        }
        else{
            genTargetViaCell();
        }
    }

    private void genTargetViaCell(){
        targetGrn = targetCell.grn;
    }
    
    private void genTargetViaMatrix() throws IOException{
        targetGrn = Grn.grnFromFile(SimStart.grnFileName);
    }

    void parallelIter(Collection<Cell> pop, CompletionService<Cell> compS, int numCores){
        int poolRemaining = 0;
        Future<Cell> fu;
        for(Cell c : pop){
            compS.submit(c);
            poolRemaining++;
        }
        while(poolRemaining > 0){
            try{
                fu = compS.take();
                Cell complete = fu.get();
                categorize(complete);
                poolRemaining--;
            }
            catch(Exception ex){
                ex.getCause().printStackTrace();
                SimStart.shutDownEverything();
                System.exit(1);
            }
        }
    }

    void perturbGenes(int numCores, CompletionService<Cell> compS) throws IOException, DeathCountException{
        Long start = System.nanoTime();
        ArrayList<Cell> clones = new ArrayList<Cell>();
        // assuming for now that the entire pop is initialized to a single genome
        // shouldnt be too hard to change this to just iterate through a full population
        // though if necessary
        Cell focalCell = liveCells.first();
        System.err.println("&&"+focalCell.genes.size());
        Cell firstClone = new Cell(focalCell, 0);
        clones.add(firstClone);
        categorize(firstClone);
        // pairwise knockouts
        for(int i = 0; i < focalCell.genes.size(); i++){
            for(int j = 0; j < focalCell.genes.size(); j++){
                Cell newClone = new Cell(focalCell, focalCell.genes.size()*i+j+1);
                newClone.spikeGene(i, -10000000);
                newClone.spikeGene(j, -10000000);
                categorize(newClone);
                clones.add(newClone);
            }
        }
        size = clones.size();
        liveCells = new ConcurrentSkipListSet<Cell>(Cell.comp); 
        liveCells.addAll(clones);

        propKill = 0;
        update();
        parallelIter(clones, compS, numCores);
        output.execute(new IterationWriter("focalGenes", dumpMapString(), baseDumpOut, true));
    }

    double[][] evalFitness(int loc, Grn ind){
        Grn tempGrn = targetGrn.copyGrn();
        tempGrn.makeExpressTraj(SimStart.internalIters, SimStart.internalIntStep, getSignalAt(loc, false));
        ind.setPairDist(tempGrn);
        return tempGrn.getPhenoTraj().getData();
    }
    
    static int getFittestInd(JsonNode pop){
        int popSize = pop.get("Size").asInt();
        int bestBabby = 0;
        double fitness = 0;
        int bestInd = 0;
        for(int i = 0; i < popSize; i++){
            JsonNode ind = pop.get("["+i+"]");
            int thisBabby = ind.get("Offspring").asInt();
            if(thisBabby > bestBabby){
                bestInd = i;
                bestBabby = thisBabby;
                fitness = ind.get("Fitness").asDouble();
            }
        }
        System.err.println(bestBabby);
        System.err.println(fitness);
        System.err.println(bestInd);
        return bestInd;
    }

    static Map<Integer, Double> setFittestRegs(JsonNode pop){
        int bestInd = getFittestInd(pop);
        JsonNode genes = pop.get("[" + bestInd + "]").get("Genes");
        Map<Integer, Double> result = new HashMap<>();
        int count = 0;
        for(JsonNode gene : genes){
            int target = gene.get("Target").asInt();
            result.put(target, result.getOrDefault(target, 0.0) + gene.get("Regulation").asDouble()*gene.get("Regulated").asDouble());
            count++;
        }
        System.err.println("**"+count);
        return result;
    }

    static JsonNode loadPop(Path popFile){
        JsonNode pop = null;
        try{
            pop = m.readTree(new GZIPInputStream(new FileInputStream(popFile.toFile())));
        }
        catch(IOException ex){
            System.err.println("Problem loading " + popFile + ". Is this actually a population file?");
            ex.getCause().printStackTrace();
            SimStart.shutDownEverything();
            System.exit(1);
        }
        return pop;
    }
        
    static String getFittestGenome(JsonNode pop){
        int bestInd = getFittestInd(pop);
        String result = "";
        result = pop.get("["+bestInd+"]").get("Genome").asText();
        return result;
    }
    
    // method for getting the environmental signal at a given location
    double[] getSignalAt(int i, boolean full){
        if(indRand)
            updateIndSignal(i);
        if(full)
            return signals.get(i);
        else{
            double[] result = new double[Gene.signalLen];
            System.arraycopy(signals.get(i), Gene.peturbLen, result, 0, Gene.signalLen);
            return result;
        }
    }

    void updateIndSignal(int i){    
        signals.set(i, genSignals(genSignalPointer(i)));
    }

    void updatePopSignals(){
        int[] signalPointer = genSignalPointer(0);
        double[] newSignals = genSignals(signalPointer);
        signals = new ArrayList<>();
        for(int i = 0; i < board.length; i++){
            signals.add(newSignals);
        }
    }

    double[] genSignals(int[] signalPointer){
        double[] newSignals = new double[Gene.signalLen+Gene.peturbLen];
        for(int i = 0; i < newSignals.length; i++){
            newSignals[i] = signal.get(i)[signalPointer[i]];
        }
        return newSignals;
    }
    
    int[] genSignalPointer(int i){
        int[] signalPointer = signalPointers.get(i);//new int[Gene.signalLen+Gene.peturbLen];
        if(mixEnv > 0 && iternum > burnIn){
            if(SimStart.iternum % mixEnv == 0){
                if(fullRand){
                    signalPointer = fullRandomize(signalPointer); 
                }
                else
                    signalPointer = linkedRandomize(signalPointer);
            }
        }
        else if(cycleEnv && iternum > burnIn){
            signalPointer = cycle();
        }
        return signalPointer;
    }
    
    int[] fullRandomize(int[] signalPointer){
        List<Integer> tempInds = new ArrayList<>(signalInds);
        Collections.shuffle(tempInds);
        int[] result = signalPointer;//new int[Gene.signalLen+Gene.peturbLen];
        for(int i = 0; i < mixNum; i++){
            int s = tempInds.get(i);
            try{
                result[s] = rand.nextInt(signal.get(s).length);
            }
            catch(Exception e){
                System.err.println(result.length);
                System.err.println(mixNum);
                throw(e);
            }
        }
        return result;
    }

    int[] linkedRandomize(int[] signalPointer){
        int[] result = signalPointer.clone();//new int[Gene.signalLen+Gene.peturbLen];
        int maxPointer = rand.nextInt(maxSigVariation);
        for(int s = 0; s < result.length; s++){
            result[s] = maxPointer % signal.get(s).length;
        }
        return result;
    }

    int[] cycle(){
        int[] result = new int[Gene.signalLen+Gene.peturbLen];
        for(int s = 0; s < result.length; s++){
            result[s] = SimStart.iternum%signal.get(s).length;
        }
        return result;
    }
 
    void run(int numI, int numCores, CompletionService<Cell> compS) throws IOException, DeathCountException{
        int poolRemaining = 0;
        Long start = System.nanoTime();
        Future<Cell> fu;
        maxIter = numI;
        if(SimStart.newOut)
            f[0].write(String.join(",", summaryMap.keySet())+"\n");
        if(SimStart.lineOut)
            f[1].write("{\n");
        while(iternum < maxIter){
            SimStart.iternum = iternum;
            long parPart = System.nanoTime();
            targetGrn.makeExpressTraj(SimStart.internalIters, SimStart.internalIntStep, signals.get(0));
            parallelIter(liveCells, compS, numCores);
            try{
                long envTest = System.nanoTime();
                update();
                f[0].write(sumString() + "\n");
                if(SimStart.lineOut){
                    f[1].write(String.format("\"[%d]\" : ", iternum) + m.writeValueAsString(fitInd.dumpMap()));
                    if(iternum < maxIter -1){
                        f[1].write(",\n");
                    }
                }
                if(iterout > 0 && iternum % iterout == 0 && dumpOut){
                    output.execute(new IterationWriter(iternum, dumpMapString(), baseDumpOut, true));
                    /*
                    Cell fittest = liveCells.last();
                    output.execute(new IterationWriter("finalGrn_" + iternum, fittest.grnString(), baseDumpOut, false));
                    output.execute(new IterationWriter("finalGenes_" + iternum, fittest.geneList(), baseDumpOut, false));
                    output.execute(new IterationWriter("finalTraj_" + iternum, fittest.grn.exprString()+"\n\n"+targetGrn.exprString(), baseDumpOut, false));
                    */
                }
                if(doaCount == liveCells.size() && iternum > 0){
                    break;
                }
            }
            catch(Exception ex){
                SimStart.shutDownEverything();
                f[0].flush();
                f[0].close();
                if(f[1] != null){
                    f[1].flush();
                    f[1].close();
                }
                throw(ex);
            }
            iternum++;
        }
        if(SimStart.lineOut)
            f[1].write("\n}\n");
        f[0].write((System.nanoTime() - start)/1000000000.+"\n");
        Cell fittest = liveCells.last();
        int maxChild = fittest.numBabby.get();
        for(Cell c : liveCells){
            if(c.numBabby.get() > maxChild){
                fittest = c;
                maxChild = c.numBabby.get();
            }
        }        
        output.execute(new IterationWriter("finalGenome", fittest.gen.toString(), baseDumpOut, false));
        //output.execute(new IterationWriter("finalGrn", fittest.grnString(), baseDumpOut, false));
        //output.execute(new IterationWriter("finalGenes", fittest.geneList(), baseDumpOut, false));
        if(netAnalysis){
            try{
                output.execute(new IterationWriter(maxIter, dumpMapString(), baseNetOut, true));
            }
            catch(Exception ex){
                ex.getCause().printStackTrace();
                SimStart.shutDownEverything();
                System.err.println("Exiting");
                System.exit(1);
            }
        }
    }

    // not including influence in this since it's a 256x256 matrix though maybe I should?
    public TreeMap<String, Object> dumpMapString() throws JsonProcessingException, IOException, OutOfMemoryError{
        TreeMap<String, Object> result = new TreeMap<String, Object>();
        result.put("Size", size);
        result.put("Signal", signal.stream().map(s -> Arrays.toString(s)).toArray());
        for(Cell c : liveCells){
            try{
                result.put("["+c.loc+"]", c.dumpMap());
            }
            catch(NullPointerException e){
                if(c == null){
                    System.err.println("c is null");
                }
                else{
                    System.err.println(c.loc);
                    System.err.println(c.dumpMap());
                }
                throw(e);
            }
        }
        return result;
    }

    public String bornMapString() throws JsonProcessingException{
        TreeMap<String, Object> result = new TreeMap<>();
        result.put("Size", size);
        result.put("Signal", signal.stream().map(s -> Arrays.toString(s)).toArray());
        for(Cell c : board){
            if(!c.doa && c.lived == 1)
            result.put("["+c.loc+"]", c.dumpMap());
        }
        return m.writeValueAsString(result);
    }

    public String sumString(){
        String result = "";
        if(!SimStart.newOut)
            result = String.format("%d %5d | "+
                       "%5d | "+
                       "%4d | "+
                       "%4d %4d %4d %4.2f | "+
                       "%4d %4d | "+
                       "%7.3g %7.3g %4d %9.5f %9.5f %9.5g %9.5f %9.5f %9.5f %9.5f | "+
                       "%7.3g %7.3g | "+
                       "%4d %4d %4d",
                       iternum, maxLen,
                       viableCount, 
                       maxGene,
                       fittestA, worstAge, maxAge, sumAge/sumSize,
                       doaCount, killCount,
                       sumFit, maxFit, (int)sumSize, minFit, medFit, sumFit/sumSize, minNormFit, medNormFit, sumNormFit/sumSize, maxNormFit,
                       fittestM, fittestR,
                       mostOff, mostAtt, mateCount);
        //Arrays.toString(breedTree.keySet().toArray()));       
        else{
            summaryMap.replace("iter", iternum+"");
            summaryMap.replace("maxChrLen", maxLen+"");
            summaryMap.replace("viableCount", viableCount+"");
            summaryMap.replace("ageOfMostFit", fittestA+"");
            summaryMap.replace("ageOfLeastFit", worstAge+"");
            summaryMap.replace("maxAge", maxAge+"");
            summaryMap.replace("avgAge", sumAge/sumSize+"");
            summaryMap.replace("doaCount", doaCount+"");
            summaryMap.replace("killCount", killCount+"");
            summaryMap.replace("maxFit", maxFit+"");
            summaryMap.replace("minFit", minFit+"");
            summaryMap.replace("medFit", medFit+"");
            summaryMap.replace("avgFit", sumFit/sumSize+"");
            summaryMap.replace("minNormFit", minNormFit+"");
            summaryMap.replace("maxNormFit", maxNormFit+"");
            summaryMap.replace("medNormFit", medNormFit+"");
            summaryMap.replace("avgNormFit", sumNormFit/sumSize+"");
            summaryMap.replace("fittestMutRate", fittestM+"");
            summaryMap.replace("fittestRecRate", fittestR+"");
            summaryMap.replace("mostOffspring", mostOff+"");
            summaryMap.replace("mostMateAttmpt", mostAtt+"");
            summaryMap.replace("mateSuccess", mateCount+"");
            result = String.join(",", summaryMap.values());
        }
        return result;
    }

    void categorize(Cell lc){
        switch(lc.state){
        case DEAD:
            deadCells.add(lc);
            break;
        case VIABLE:
            viableCellsList.add(lc);
            break;
        default:
            break;
        }
    }

    /*
    double randomizeSignal(int s){
    return Math.min(Math.max((0.5-rand.nextDouble()) * randAmp * maxSignal[s] + maxSignal[s],0), 2*maxSignal[s]);
    }
    */
    

    void update() throws IOException, DeathCountException{
        updatePopSignals();

        if(MatrixGrn.seenResults.size() > 10000)
            MatrixGrn.seenResults.clear();
        if(iternum == 20000)
            EquationGrn.seenResults.clear();
        // Making breed tree before we kill anything.
        // This will let us do 100% replacement if we want to.
        // It also means that each new generation is created
        // from the same size parent population regardless
        // of the proportion replacement
        mateCount = 0;
        for(Cell c : viableCellsList){
            c.gen.lock();
            if(c.mated.get())
                mateCount++;
            c.mated.set(false);
            // because the "previous" cell is the one reproducing at a cells location when it is in its first
            // iteration, any reproduction at that location "belongs" to the previous cell
            // Need to do this before making viableCells because it is a list that is sorted in part by the
            // number of offspring an individual has. If we change that after making the list, we may not be able
            // to find the individual anymore.
            if(c.lived == 1){
                c.numBabby = new AtomicInteger();
                c.numAttempt = new AtomicInteger();
            }

        }

        viableCells.clear();
        viableCells.addAll(viableCellsList);
        viableCellsList.clear();

        makeBreedTree(viableCells);

        board = nextBoard;
        nextBoard = new Cell[size];
        doaCount = deadCells.size();
        double needKill = killGoal - doaCount;
        int checkC = 0;
        while(viableCells.size() > needKill && deadCells.size() < killGoal){
            Collection<Cell> tempCells = killFrom(viableCells, needKill, true);
            viableCells.removeAll(tempCells);
            needKill -= tempCells.size();
            deadCells.addAll(tempCells);
            checkC++;       
            if(needKill + deadCells.size() != killGoal || deadCells.size() + viableCells.size() != size){
                System.err.println(needKill+"+"+deadCells.size()+"!="+killGoal+"?");
                System.err.println(deadCells.size()+"+"+viableCells.size()+"!="+size+"?");
                throw new DeathCountException(killGoal, needKill + deadCells.size());
            }
        }
        if(needKill > 0 && viableCells.size() <= needKill){
            deadCells.addAll(viableCells);
            needKill -= viableCells.size();
            viableCells.clear();
            if(needKill + deadCells.size() != killGoal || deadCells.size() + viableCells.size() != size){
                System.err.println(needKill+ " + " + deadCells.size() + " =? " + killGoal);
                System.err.println(deadCells.size() + " + " + viableCells.size() + " =? " + size);
                throw new DeathCountException(killGoal, needKill + deadCells.size());
            }
        }

        if(doaCount < killGoal && killGoal != deadCells.size() && viableCells.size() > needKill || deadCells.size() + viableCells.size() != size){
            System.err.println(doaCount < killGoal);
            System.err.println(killGoal != deadCells.size());
            System.err.println(viableCells.size() > needKill);
            System.err.println(deadCells.size() + viableCells.size() != size);
            System.err.println();
            System.err.println(viableCells.size());
            System.err.println(deadCells.size());
            System.err.println(size);
            System.err.println(needKill);
            System.err.println(liveCells.size());
            System.err.println(doaCount);
            throw new DeathCountException(killGoal, deadCells.size());
        }
        killCount = deadCells.size();
        // Actually kill them.
        for(Cell lc : deadCells){
            lc.killed = true;
        }
        //makeBreedTree(viableCells);
        // We want to calculate population stats based on all the cells which were alive in the previous
        // iteration.
        viableCells.addAll(deadCells);
        // finding values of summary stats for simulation runtime string
        sumAge = 0;
        sumPhen = 0;
        maxGene = 0;
        maxUsed = 0;
        maxLen = 0;
        maxAge = 0;
        minAge = Integer.MAX_VALUE;
        sumNC = 0;
        sumSize = 0;
        maxFit = Double.NEGATIVE_INFINITY;
        maxNormFit = Double.NEGATIVE_INFINITY;
        sumFit = 0;
        sumNormFit = 0;
        fittestA = 0;
        fittestM = 0;
        fittestR = 0;
        worstAge = 0;
        maxOff = 0;
        fitInd = viableCells.first();
        minFit = Double.POSITIVE_INFINITY;
        minNormFit = Double.POSITIVE_INFINITY;
        mostOff = 0;
        mostAtt = 0;    
        int count = 0;
        for(Cell tc : viableCells){
            int thisGene = 0;
            int thisUsed = 0;
            double thisMolSum = 0;


            if(tc.lived < 0){
                continue;
            }

            for(Chromosome chr: tc.gen.gen){
                if(chr.len > maxLen)
                    maxLen = chr.len;
            }
            if(tc.fitness > maxFit){
                maxFit = tc.fitness;
                maxNormFit = tc.normFit;
                worstAge = tc.lived;
            }
            if(tc.lived > maxAge){
                maxAge = tc.lived;
            }
            if(tc.lived < minAge){
                minAge = tc.lived;
            }
            if(tc.fitness < minFit){
                minFit = tc.fitness;
                minNormFit = tc.normFit;
                if(minFit < 0.00001 && minNormFit > 0.5){
                    System.err.println("is this just fantasy?");
                    System.err.println(minFit + "\t" + minNormFit + "\t" + tc.lived + "\t" + tc.numBabby.get() + "\t" + tc.numAttempt.get()+"\t"+tc.state+"\t"+tc.doa);
                    SimStart.shutDownEverything();
                    System.exit(1);
                }
                //sumPhen = tc.ps;
                //sumPhen = tc.normFit;
            }
            if(tc.numBabby.get() > mostOff){
                mostOff = tc.numBabby.get();
                mostAtt = tc.numAttempt.get();
                mostOffd = Arrays.toString(tc.offDist.toArray());
                mostAttd = Arrays.toString(tc.attDist.toArray());
                fittestA = tc.lived;
                fittestM = tc.ptRate;
                fittestR = tc.rcRate;
                maxUnique = tc.uniqueGenes.size();
                maxUsed = tc.genesUsed;
                maxGene = tc.numGene;
            }
            if(count == viableCells.size()/2){
                medFit = tc.fitness;
                medNormFit = tc.normFit;
            }

            if(tc.numBabby.get() > maxOff ||
               (tc.numBabby.get() == maxOff && tc.fitness < fitInd.fitness)){
                fitInd = tc;
            }
            
            if(tc.generation > numGen)
                numGen = tc.generation;
            
            sumSize++;
            sumFit += tc.fitness;
            sumNormFit += tc.normFit;
            sumAge += tc.lived;
            sumNC += tc.numC;
            count++;
        }
        
        
        viableCount = viableCells.size();
        
        viableCells.clear();
        deadCells.clear();
    }

    private HashSet<Cell> killFrom(NavigableSet<Cell> pop, double needKill, boolean unused){
        long start = System.nanoTime();
        ArrayList<Long> checks = new ArrayList<>();
        HashSet<Cell> deadCells = new HashSet<>();
        ArrayList<Cell> popArray = new ArrayList<Cell>(pop);
        Collections.shuffle(popArray);
        TreeMap<Double, Cell> dieMap = new TreeMap<>();
        double fitAvg = 0;
        double fitTot = 0;
        checks.add(System.nanoTime());
        boolean allSame = true;
        double prevFit = Double.POSITIVE_INFINITY;
        medFit = Double.POSITIVE_INFINITY;
        int count = 0;
        for(Cell lc : pop){
            if(Double.isInfinite(prevFit)){
                prevFit = lc.fitness;
            }
            if(allSame && lc.fitness != prevFit){
                allSame = false;
            }
            if(count == pop.size()/2){
                medFit = lc.fitness;
            }
            count++;
            prevFit = lc.fitness;
            fitTot += lc.fitness;
        }   
        fitAvg = fitTot/pop.size();
        checks.add(System.nanoTime());
        double modFit = (medFit > 0)? medFit : 0.00001;
        //double modFit = (fitAvg > 0)? fitAvg : 0.00001;
        //modFit = 50;
        int count1 = 0;
        int count2 = 0;
        for(Cell lc : popArray){
            if(dieMap.size() < needKill){
                if(allSame){
                    dieMap.put(lc.fitness - rand.nextDouble(), lc);
                }
                else{
                    dieMap.put(rand.nextDouble()*modFit*killSelect + lc.fitness, lc);
                }
            }
            else{
                double mod = rand.nextDouble()*modFit*killSelect + lc.fitness;
                if(mod > dieMap.firstKey()){
                    dieMap.pollFirstEntry();
                    dieMap.put(mod, lc);
                }
            }
        }

        //TreeMap<Double, Cell> dieMap = new TreeMap<>(dieMapH);
        checks.add(System.nanoTime());
        deadCells.addAll(dieMap.values());
        // this happens if things are nearly identical so standard deviation (s) is very very close to zero and 
        // results in multiple things being put in the same "bin"
        if(dieMap.size() == 0 && dieMap.lastKey() == 0){
            for(Cell lc : pop){
            deadCells.add(lc);
            if(deadCells.size() == needKill)
                break;
            }
        }

        checks.add(System.nanoTime());
        double div = 1000000000.;
        double totTime = (checks.get(checks.size()-1)-start)/div;
        String timeString = "Started:\t" + start/div;
        double check0Time = (checks.get(0) - start)/div;
        timeString += "\nKill0:\t" + check0Time + "\t" + check0Time/totTime;
        for(int i = 1; i < checks.size(); i++){
            double thisTime = (checks.get(i) - checks.get(i-1))/div;
            timeString += "\nKill" + i + ":\t" + thisTime + "\t" + thisTime/totTime;
        }
        timeString += "\nTotal:\t" + totTime;
        //System.err.println(timeString);
        return deadCells;
    }

    private Collection<Cell> killFrom(Set<Cell> pop, double needKill){
        long start = System.nanoTime();
        TreeMap<Double, Cell> dieMap = new TreeMap<>();
        double[] fitArry = new double[pop.size()];
        ArrayList<Cell> popArray = new ArrayList<Cell>(pop);
        double ss = -killSelect; // negative because high fitness values = bad
        double fitAvg = 0;
        double prevFit = Double.POSITIVE_INFINITY;
        boolean allEqual = true;
        for(int i = 0; i < popArray.size(); i++){
            Cell lc = popArray.get(i);
            fitArry[i] = lc.fitness;
            fitAvg += fitArry[i];
            if(Double.isInfinite(prevFit)){
                prevFit = lc.fitness;
            }
            if(allEqual && lc.fitness != prevFit){
                allEqual = false;
            }
            else{
                prevFit = lc.fitness;
            }
        }
        fitAvg /= fitArry.length;

        // Selecting which cells should die. Transforming fitness using
        // an exponential function to penelize cells with very small fitness
        // significantly more than those with less low fitness. 
        double[] dieIntervals = new double[fitArry.length];

        dieIntervals[0] = Math.exp(ss*(1 - fitArry[0]/fitAvg));
        double dieTotal = dieIntervals[0];
        double dieMin = Double.POSITIVE_INFINITY;
        // Create weighted intervals
        for(int i = 1; i < fitArry.length; i++){
            dieIntervals[i] = Math.exp(ss*(1 - fitArry[i]/fitAvg));
            if(dieIntervals[i] < dieMin){
                dieMin = dieIntervals[i];
            }
            dieTotal += dieIntervals[i];
        }
        double dieAvg = dieTotal/dieIntervals.length;

        double s = 0.0;
        for(double d : dieIntervals){
            s += Math.pow(d - dieAvg, 2);
        }
        s = Math.sqrt(s/dieIntervals.length);
        double ks2 = 1;
        for(int i = 0; i < dieIntervals.length; i++){       
            if(dieMap.size() < needKill){
                if(allEqual){
                    dieMap.put(dieIntervals[i] + rand.nextDouble(), popArray.get(i));
                }
                else
                    dieMap.put(rand.nextDouble()*dieMin*ks2 + dieIntervals[i], popArray.get(i));
            }
            else{
                double mod = rand.nextDouble()*dieMin*ks2 + dieIntervals[i];
                if(mod > dieMap.firstKey()){
                    dieMap.pollFirstEntry();
                    dieMap.put(mod, popArray.get(i));
                }
            }
        }
        return dieMap.values();
    }

    private void makeBreedTree(NavigableSet<Cell> pop){
        breedTree.clear();
        if(pop.size() == 0){
            return;
        }
        double fitAvg = 0;
        double fitTot = 0;
        double fitMax = pop.stream().mapToDouble(Cell::getFitness).max().orElse(0);
        int avgCount = 0;
        boolean allEqual = true;
        double prevFit = Double.POSITIVE_INFINITY;
        double medFit = Double.POSITIVE_INFINITY;
        int count = 0;
        for(Cell lc : pop){
            if(Double.isInfinite(prevFit)){
                prevFit = lc.fitness;
            }
            if(allEqual && lc.fitness != prevFit){
                allEqual = false;
            }
            else{
                prevFit = lc.fitness;
            }
            if(count == pop.size()/2){
                medFit = lc.fitness;
            }
            count++;
            fitTot += lc.fitness;
            if(lc.killed){
                System.err.println("Killed cell got into makeBreedTree");
                System.err.println(pop.size());
                Thread.currentThread().dumpStack();
                SimStart.shutDownEverything();
                System.exit(1);
            }
            // If the average energy level after normalization is too low, when we
            // use the exponential function later on the highest energy individual, we
            // can end up doing math on infinite numbers. If after normalization, a value
            // is less than 1/100th of the maximum, It's probably safe to assume that it will never be
            // selected so we just don't worry about it. This will increase selection for higher
            // fitness individuals relative to population size.
            /*
            if(lc.energy > 0.01)
            avgCount++;
            */
        }
        fitAvg = fitTot/pop.size();
        double[] breedIntervals = new double[pop.size()];
        if(pop.size() == 1){
            breedTree.put(1.0, (Cell)pop.toArray()[0]);
            return;
        }
        if(allEqual){
            count = 0;
            ArrayList<Cell> tempArr = new ArrayList<Cell>(pop);
            Collections.shuffle(tempArr);
            double equalPartition = 1/(tempArr.size() + 0.);
            double partCount = equalPartition;
            for(Cell c : tempArr){
                breedTree.put(partCount, c);
                partCount += equalPartition;
            }
            return;
        }
        double breedTotal = 0;
        double bs = repSelect;
        Cell[] popArray = pop.toArray(new Cell[0]);
        double breedMax = 0;
        if(medFit < 0.00000001)
            medFit = 0.00000001; // just needs to be something sufficiently small but non-zero
        for(int i = 0; i < popArray.length; i++){
            //breedIntervals[i] = Math.exp(-bs*(1-popArray[i].fitness/fitAvg));
            breedIntervals[i] = Math.exp(-bs*(1-popArray[i].fitness/medFit));
            if(breedIntervals[i] > breedMax)
                breedMax = breedIntervals[i];
        }
        for(int i = 0; i < breedIntervals.length; i++){
            //breedIntervals[i] = breedIntervals[i]/breedMax;
            breedTotal += breedIntervals[i];
        }

        double expMod = 0;
        int infCount = 1;
        if(Double.isInfinite(breedTotal)){
            if(!Double.isInfinite(breedMax)){
                breedTotal = 0;
                for(int i = 0; i < breedIntervals.length; i++){
                    if(Double.isInfinite(breedTotal + breedIntervals[i])){
                        infCount++;
                        breedTotal -= Double.MAX_VALUE;
                    }
                    breedTotal += breedIntervals[i];
                    if(breedTotal < 0){
                        System.err.println("you were wrong");
                        System.exit(1);
                    }
                    if(Double.isInfinite(breedTotal)){
                        System.err.println("we boned");
                        //System.err.println(breedIntervals[i] + "\t" + popArray[i].fitness + "\t" + fitAvg);
                        System.err.println(breedIntervals[i] + "\t" + popArray[i].fitness + "\t" + medFit);
                        System.exit(1);
                    }
                }
                expMod = -1*Math.log(1.0/(double)(pop.size() * infCount));
                breedMax = 0;
                breedTotal = 0;
                for(int i = 0; i < popArray.length; i++){
                    //breedIntervals[i] = Math.exp(-bs*(1-popArray[i].fitness/fitAvg)-expMod);
                    breedIntervals[i] = Math.exp(-bs*(1-popArray[i].fitness/medFit)-expMod);
                    if(breedIntervals[i] > breedMax)
                        breedMax = breedIntervals[i];
                }
                for(int i = 0; i < breedIntervals.length; i++){
                    breedTotal += breedIntervals[i];
                }
            }
            // some individuals are so much more fit than the average that we'll just pretend that they're infinite
            else{
                breedTotal = 0;
                for(int i = 0; i < breedIntervals.length; i++){
                    if(Double.isInfinite(breedIntervals[i])){
                        breedIntervals[i] = popArray[i].fitness;
                        breedTotal += popArray[i].fitness;
                    }
                    else{
                        breedIntervals[i] = 0;
                    }
                }
            }
        }

        //Arrays.sort(breedIntervals);
        
        breedIntervals[0] = breedIntervals[0]/breedTotal;
        breedTree.put(breedIntervals[0], popArray[0]);

        for(int i = 1; i < breedIntervals.length-1; i++){
            breedIntervals[i] = breedIntervals[i]/breedTotal + breedIntervals[i-1];
            breedTree.putIfAbsent(breedIntervals[i], popArray[i]);
        }

        // due to floating point rounding it is possible that the last value is 0.9999999999999999923423 or
        // something like that. This just checks to see if the last interval is "close enough". If it isn't,
        // there is a bug we must fix.
        double lastInterval = breedIntervals[breedIntervals.length - 1]/breedTotal + breedIntervals[breedIntervals.length - 2];
        if(lastInterval < 1 && lastInterval + 0.0000001 >= 1){
            lastInterval = 1;
        }
        breedIntervals[breedIntervals.length - 1] = lastInterval;
        breedTree.putIfAbsent(lastInterval, popArray[breedIntervals.length-1]);
        if(breedTree.firstKey() == breedTree.lastKey() && breedTree.firstKey() == 0 || breedTree.lastKey() < 1 || breedTree.lastKey() > 1.0001){
            for(int i = 0; i < breedIntervals.length; i++){
                //double temp = Math.exp(-bs*(1-(popArray[i].fitness+1)/fitAvg)-expMod);
                double temp = Math.exp(-bs*(1-(popArray[i].fitness+1)/medFit)-expMod);
                //double temp2 = Math.exp(-bs*(1-(popArray[i].fitness)/fitAvg));
                double temp2 = Math.exp(-bs*(1-(popArray[i].fitness)/medFit));
                //System.err.println(i + "\t" + breedIntervals[i] + "\t" + popArray[i].fitness/fitMax  + "\t" +  (-bs*(1-(popArray[i].fitness)/fitAvg)+expMod) + "\t" + temp2+ "\t" + temp + "\t" + temp/breedTotal);
                System.err.println(i + "\t" + breedIntervals[i] + "\t" + popArray[i].fitness/fitMax  + "\t" +  (-bs*(1-(popArray[i].fitness)/medFit)+expMod) + "\t" + temp2+ "\t" + temp + "\t" + temp/breedTotal);
            }
            //System.err.println("~~~" + pop.size() + "\t" + fitAvg + "\t" + fitMax + "\t" + expMod + "\t" + breedTotal + "\t" + infCount + "\t" + 1./(infCount * pop.size()) + "\t" + breedTree.lastKey());
            System.err.println("~~~" + pop.size() + "\t" + medFit + "\t" + fitMax + "\t" + expMod + "\t" + breedTotal + "\t" + infCount + "\t" + 1./(infCount * pop.size()) + "\t" + breedTree.lastKey());
            System.exit(1);
        }
    }
}


class DeathCountException extends Exception{
    DeathCountException(double kg, double nk){
    super(kg + " is goal, but killing " + nk);
    }
}
