Grn-Evo - Simulating gene regulatory network evolution through simulated populations
=======
Studying gene networks and their evolution is hard. Inferring real networks is a daunting task
that requires substantial data and sophisticated statistical methods. As such, our ability to study the evolution of biological networks is limited. Computational methods, then, are a compelling avenue explore and understand how gene networks might evolve. Most such methods are either entirely analytic or are constructed with a lot of assumptions aimed at making their results easier to characterize. Here I introduce a new simulation framework called grn-evo that is more sophisticated than existing methods, and argue that the complexity of the resulting simulations is an asset that can be used to more deeply explore open questions in network evolution than existing methods.

This framework was used for my Ph.D thesis which can be found at [http://digitallibrary.usc.edu/](http://digitallibrary.usc.edu/) by searching for its title: "Investigating the evolution of gene networks through simulated populations". Chapter 1 covers in detail how the simulation works. Refer to it (or the code directly) for a more complete description of what is going on than is present in this README.

On this page:

* [Getting Started](#markdown-header-getting-started)
	* [Requirements](#markdown-header-requirements)

	* [Installation](#markdown-header-installationcompilation)

* [Required Files](#markdown-header-required-files)
	* [Environment File](#markdown-header-environment-file)

	* [Genome File](#markdown-header-genome-file)

	* [Target File](#markdown-header-target-file)


* [Running](#markdown-header-running)
	* [Command Line Options](#markdown-header-command-line-options)

	* [Output](#markdown-header-output)
		* [Summary Statistics](#markdown-header-summary-statistics)

		* [Population Snapshots](#markdown-header-population-snapshots)

		* [Settings](#markdown-header-settings)

	* [Examples](#markdown-header-examples)


# Getting Started

## Requirements
Java version 8 or later

[Apache Commons Math 3.6](https://commons.apache.org/proper/commons-math/) (for matrix math and numerical ODE integrator)

[Apache Commons Java Expression Language (JEXL) 3.1](https://commons.apache.org/proper/commons-jexl/) (for reading expressions from file)

[FasterXML/jackson 2.7 core](https://github.com/FasterXML/jackson-core) (for JSON i/o)

[FasterXML/jackson 2.7 databind](https://github.com/FasterXML/jackson-databind) (for JSON i/o)

[FasterXML/jackson 2.7 annotations](https://github.com/FasterXML/jackson-annotations) (for JSON i/o)

Note: newer versions of the above libraries *may* work, but they have not been tested.

## Installation/Compilation
Clone this repository using `git clone git@bitbucket.org:conow/grn-evo.git` and download/install the required packages linked above.

 A quick and dirty way install the required packages is to download the relevant binary jars, unpack them with `jar xf` and put the resulting `com` or `org` folders into the source directory of `grn-evo`. Note that this will only make these libraries visible to `javac` when it is invoked in the `grn-evo` folder, so if you wish to use them elsewhere it is recommended to set up a java library path properly and install packages in the relevant location.

Once you have the Apache Commons Math and jackson libraries installed and visible to `javac`'s compile path, you should be able to compile this repository with a simple `javac *.java` in the `grn-evo` folder.

# Required Files

## Environment File
Environment files are plaintext files that contain rows of numbers where elements are separated by white space. Each row represents the distribution of values that either an environmental signal may take or by how much initial values for phenotype signals may be perturbed. There needs to be at least one value per row, but there is no maximum row length, and rows may have different lengths. The number of rows needs to be at least as great as the sum of the values specified by using the `-sl` and `-bl` commands (default 2 and 0 respectively). The first "`sl`" lines refer to distributions of environmental signals, while the remaining "`bl`" lines refer to distributions of perturbations to initial phenotype values.

Briefly, the environmental signals can be "sensed" by certain types of genes, which affect their output and thus their impact on other genes. Additionally, the targets of selection (described in the [target file](#markdown-header-target-file) section) may shift depending on what's going on in the environment.

Meanwhile, the values used for perturbation are chosen randomly to be added to initial phenotype values used in the initial value problem discussed briefly in the [target file](#markdown-header-target-file) section and in more detail in my dissertation.

`signal0.1_1` is a simple example of this type of file that specifies up to 12 signals, where signal distributions in odd rows and even rows are identical respectively. It is also the environment file that the simulation looks for by default.

## Genome File
Genome files consist of two lines, where each line represents a chromosome. The characters allowed in each "chromosome" line are any number of the digits 0-3, as well as one required @ character that acts as a "centromere" used to align chromosomes during reproduction for crossing over. A pattern of `0001` specifies the start of a gene, with the next 48 digits specifying what exactly the gene does.

While some parts of this simulation support additional chromosomes, the simulation as a whole is not designed to consider them.

`seed0.gen` is an example genome, and what the simulation looks for by default.

## Target File
The target file can be formatted in two ways; either by declaring a set of expressions (using JEXL) that describe how the target of selection for each phenotype responds to what's going on in the environment, or as a matrix that specifies a system of differential equations that define an initial value problem analogous to how gene regulatory networks are defined within the simulation. The first line in the file is used to specify which type of file this is. If the first line is `## equation` then it is the former case, while if it is `## matrix` it is the latter. In either case, at every iteration a series of values are generated for each phenotype that occur over a time period of 0 to 5, with a time step of 0.25.

In `equation` target files, the second line indicates how many environmental signals are needed. The remainder of the file is a bracketed, comma separated list of expressions where each expression describes how one target phenotype is determined. Environmental signals are retrieved using `e[n]` where `n` describes which signal to index, and time is represented by the variable `t`. 

See `testScript` for an example `equation` target file.

In `matrix` target files, each row of whitespace separated numbers except the last represents the derivative of some function of all the signals (internal and environmental) which in turn determines the gene regulatory output for the signal whose index matches a given row. This matrix plus environmental signals and genes which produce "initial" signals yields an initial value problem which can be numerically integrated to describe the behavior of phenotypic output over some period of time. If the matrix is `n` columns it must have `n+1` rows. If there are `m` environmental signals (i.e. specified by the `-sl` argument), then the first `n - m` rows specify how the `target` gene regulatory network should behave given environmental input and initial internal conditions. Of these rows, the last `l` (i.e. specified by the `-pl` argument) of them specify the output of "phenotype" genes which are the signals that are "visible" to selection and are what will be compared to the output of networks produced by the simulation. The next `m` rows represent the environment, and should be left as all 0s since this version of the simulation does not allow organisms to influence their environment. The last row specifies what the initial conditions for each signal value should be. Note: environmental signals in this row are added to whatever values are specified in the [environment file](#markdown-header-environment-file).

See `t3`, `t4`, and `test.sin` for example `matrix` target files.

![sin example](https://bitbucket.org/conow/grn-evo/raw/b30c5bde0df258df4dafda0529dd5063367ac383/docs/images/sin.png)

*Example results. Solid lines are phenotypic output produced by an individual taken from the final generation of a population adapted to produce the `sin` phenotype for the environmental signal indicated above each plot. The dashed lines are the exact curve being selected for each phenotype. This population evolved with a cyclical environment and mutation rate at at `1e-3` and recombination rate at `1e-5`.*

# Running
The `main` class for this software is in `SimStart.java`, so run by invoking `java SimStart` (with whatever java flags are appropriate for your machine, e.g. max heap allocation) plus command line options enumerated next. Examples are given at the end of this section.

## Output

### Summary Statistics
The following summary statistics are calculated across the population:
* iteration number

* length of the longest chromosome

* the number of viable individuals in the population

* the age of 
	* the fittest individual

	* the oldest individual

	* the youngest individual

	* the population average

* counts of
	* "dead on arrival" (doa) individuals (i.e. number of "failed" reproduction attempts)

	* individuals killed

* the fitness of 
	* the most fit individual

	* the least fit individual

	* the population average

	* the population median

* for the fittest individual, the rate of: (these are useful when mutable rates are turned on, which requires manually editing reproduction in Cell.java)
	* mutation

	* recombination

* reproduction counts:
	* most offspring of any individual alive

	* most attempted matings of any individual alive (note: individuals may mate many times per iteration)

	* number of unique individuals who successfully mated this iteration

These statistics are output to standard out if the `-do` option is not specified, and to a file called `stdOut` in the "dump out" folder if a path is specified with `-do`.

When output to standard out, these statistics are space separated in the order listed above for ease of human readability, along with `|` characters to break up the top-level "sections" in the bullet points to make the columns easier to track at a glance.

![](https://bitbucket.org/conow/grn-evo/raw/a48ff5390c154f2540013d0718361225d136e6f8/docs/images/old%20out.png)

*First five lines of output when summary statistics are written to the console.*

When output is written to a `stdOut` file, it is instead formatted as a table of comma separated values with a header in order to facilitate easier incorporation into data analysis tools.

![](https://bitbucket.org/conow/grn-evo/raw/a48ff5390c154f2540013d0718361225d136e6f8/docs/images/new%20out.png)

*Header plus first five lines of stdOut when it is written to file or the `-no` flag is used.*

This output can be used to analyze high level trends in evolution across various simulation conditions:

![sin cycle smallest fit](https://bitbucket.org/conow/grn-evo/raw/b30c5bde0df258df4dafda0529dd5063367ac383/docs/images/sin_cycle_smallestFitTab.png)

*Example plot of taking the values of the "most fit individual" column of the stdOut file. Shown is the minimum distance from the target phenotype over 5,000 iterations for simulations with a cycling environment and selection for phenotype `sin`. Data is subset by mutation rate and recombination rate. Each subplot has ten replicate runs, where each run is a different color line and the black line is the mean across all runs.*

### Population Snapshots
When the `-do` option is specified with a path, "snapshots" of the population are saved every 1000 iterations (including the initial population). These files are automatically gzipped and are named `<number>.out.gz` where `<number>` is the iteration on which the population was "dumped".

This file (if unzipped) is a plaintext description of the environment and entire population formatted in JSON. The following attributes are listed in alphabetical order within a "level" of the organization of the JSON object (environment > individual > gene).

At the top level are two environmental descriptions:
* Signal: description of the signal/perturbation vectors used to run the simulation (i.e. the contents of the [file](#markdown-header-environmental-file) specified by the `-si` flag)

* Size: the size of the population

Next is a list of `"[<number>]"`s where each `<number>` is a given individual in the population at that point in time. Each individual "possesses" a dictionary that describes that individual. In order these traits are:
* Age

* AttTraj: a list which enumerates the number of mating (att)empts the individual has made over each iteration it has been alive

* FitTraj: a list which enumerates the fitness values the individual has had each iteration it has been alive

* Fitness: the fitness of the individual at the iteration the snapshot was "dumped"

* Generation

* Genes: A list of dictionaries, each of which describes an individual gene. Gene attributes are as follows:
	* BindingSite: A list of values that indicate what signals influence the expression level of this gene. Negative values indicate genes that the gene is either influenced by the environment or a phenotype expression level.

	* TotTarg: The number of other genes this gene directly influences

	* BaseReg: The magnitude to which the expression level of this gene is regulated

	* Targets: A list of values that indicate the signals that this gene influences (negative values indicate a gene is influencing a phenotype that is visible to selection imposed by the [target file](#markdown-header-target-file))

	* Direction: Indicates whether this gene up or down regulates downstream targets. 1 is positive, -1 is negative, and 0 means that this gene doesn't actually influence anything.

	* BaseExpr: The basal expression level of this gene

	* Type: There are five types of genes
		* 0: Influenced by either the environment or the phenotypic expression, and influence some number of internal signals (not phenotypes)

		* 1: Influenced by the internal state of the cell, and influence exactly one phenotypic expression level

		* 2: Influenced by either the environment or the phenotypic expression, and influence exactly one phenotypic expression

		* 3: Influenced by the internal state of the cell, and in turn influence some number of internal signals

		* 4: Have a basal expression level and may influence either one internal signal or one phenotype

	* Target: A single value encoded in the gene sequence that either directly indicates the target of regulation for this gene if its type is 1, 2, or 4. For types 0 and 3, this value along with "Specificity" determine the up to 4 internal signals that can be influenced

	* Specificity: Allows a range of specificity/promiscuity in "active site" of genes. The following code snippet shows how this value is used along with "Target" to determine the range of internal targets of regulation for genes of type 0 and 3 (refer to the function `genTargets` in `Gene.java` to see this snippet in context):

            TreeSet<Integer> targets = new TreeSet<Integer>();
            targets.add(target);
            for(int i = 0; i < 7; i += 2){
                targets.add(target^((1<<i)&spec));
                targets.add(target^((2<<i)&spec));
                targets.add(target^((3<<i)&spec));
            }

	* Used: boolean value that indicates whether anything targets this gene's binding site(s)

	* Locs: A pair of values that indicate where in a genome a given gene is located. The first value corresponds to the "first" homologous chromosome, while the second refers to the other. Whichever value is not -1 indicates which chromosome this gene copy occurs in, and that value is its location. A negative value means that its start site occurs that many bases before the centromere (the single `@` character each chromosome has), while a positive value means that the start site occurs that many bases after the centromere. Since gene lengths are larger than 1, no legitimate start site can be -1. Note: if multiple identical gene copies exist, each copy will be an independent "entry" in the list of genes. They will *not* be collapsed into a single entry with multiple locations listed.

	* Regulation: The effect this gene has on its target(s). Obtained by multiplying "BaseReg" by "Direction"

* Genome: The string representation of the individuals genome in base 4

* Grn: The "matrix" representation of the gene regulatory network specified by genes interacting by regulating signals

* InitialVals: The initial values specified by genes of type 4 used to "start" the initial value problem that the grn matrix integrates over

* MutRate: Mutation rate

* NormFitTraj: Values from the FitTraj normalized by dividing by what the values would be if the organism had a completely non-functional gene regulatory network. This is mainly useful when comparing simulations with dramatically different targets of selection.

* NumMating: The number of times this individual has been involved in mating (including unsuccessful attempts)

* OffTraj: A list which shows the number of offspring this individual has produced each iteration of its life

* Offspring: Total number of offspring this individual has produced (sum of OffTraj)

* PairTraj: List of lists which shows the pairwise euclidean distance of each phenotype signal from the target phenotype for each iteration this individual has been alive

* PhenTraj: List of lists which shows the raw phenotype values this individual has produced each iteration it has been alive

* RecRate: Recombination rate

* SigTraj: List of environmental signal values that this individual was exposed to over its lifetime

* TarTraj: List of lists which shows the target phenotype values this individual has been selected to produce for each iteration it has been alive

### Settings
In addition to population snapshots when `-do` is specified, the simulation also outputs a file called `settings` which contains a JSON formatted string that specifies the settings used to run the simulation. This file may be used with the `-sf` command to run additional simulations with the same setup.

## Command Line Options
Options which require additional information need to have a space between the flag and the extra information. For example, to dump snapshots to /your/favorite/directory/ you would use `-do /your/favorite/directory/`. All of them have "sane" default values.

* `-do` followed by a path specifies the path to dump population snapshots every 1,000 iterations. Snapshots will not be saved if this option is not used. Snapshots are saved as standard JSON files, with key-value pairs specified in the [output](#markdown-header-output) section.

* `-sf` followed by a path to a file specifies a file that has a JSON string which contains a number of settings used in a previous run. These files are output whenever `-do` is used. When loading from a settings file, individual settings may be overridden by specifying different values via command line arguments.

* `-ni` followed by an integer specifies the number of iterations the simulation should run for including the initial 0th iteration. Default is 50,001 (i.e. 50,000 iterations after initialization).

* `-bp` followed by an integer specifies the "burn-in period" for random environments. This is the number of iterations that the population is allowed to evolve before stochasticity kicks in, so that some initial functional gene regulator network can evolve under "easier" stable conditions. Default value is 0.

* `-bs` followed by an integer specifies the population (board) size. This defaults to 1000.

* `-re` followed by two integers specify that a randomized environment should be used, as well as the type of randomization. The magnitude of the first integer specifies how frequently in iterations the environment should be randomized (e.g. a value of 5 means the environment will be randomized every 5 iterations after the burn in period has completed). The sign of the first integer indicates whether each signal should be randomized independently (positive value), or if they should be linked per the file that describes the distribution of environmental signals (negative). See the section on [environment files](#markdown-header-environment-file) for more information. In the case of independent randomization of signals, the second integer specifies how many environmental signals should be randomized at a time. The second integer does nothing if the first one is negative (i.e. signals are linked). Randomization is off by default.

* `-ri` a flag which specifies that environmental signal randomization should be per individual. That is, the signal that each individual "sees" is randomized independently per the parameters given by the `-re` option. Without this, each individual in the population "sees" the exact same signal, even if it is randomized overall.

* `-ce` a flag which indicates that the environment should change, but in a predictable, cyclical way. This works by starting each environmental signal at the first column specified in the [environment file](#markdown-header-environment-file), and then stepping through columns, wrapping around to the beginning again once a row for a given signal has been completely enumerated.

* `-pk` followed by a floating point number in the range [0, 1] specifies the proportion of the population to kill (and replace) each iteration. Defaults value is 0.0625.

* `-gf` followed by a path that specifies the [genome file](#markdown-header-genome-file). Defaults to `seed0.gen` in the current directory.

* `-si` followed by a path that specifies the [environment file](#markdown-header-environment-file) (signal file). Defaults to `signal0.1_1` in the current directory.

* `-tf` followed by a path specifies the [target file](#markdown-header-target-file). Defaults to `t3` in the current directory.

* `-mr` followed by a floating point number that specifies the per-base mutation rate. Default value is 1e-4.

* `-rr` followed by a floating point number that specifies the per-base recombination rate. Default value is 1e-4.

* `-nt` followed by an integer specifies the number of threads to use for the parallel portion of the simulation. If not specified, the program attempts to identify the number of cores reserved in a compute node on the USC HPC cluster, and if it can't it defaults to 4.

* `-ks` followed by a floating point number specifies the strength of selection when deciding which individuals die in a given iteration. 

* `-sc` followed by a floating point number specifies the strength of selection when deciding which individuals reproduce in a given iteration. 

* `-fn` followed by a number specifies which subfolder a given simulation should write to. This is useful when running parallel jobs to ensure that two jobs don't try to write to the same folder. If jobs aren't being run in parallel, this isn't necessary as the program detects how many subfolders already exist for the parameter set and makes the new folder that many plus one.

* `-sl` followed by a positive integer specifies the number of signals to use from the [environment file](#markdown-header-environment-file). This value defaults to 2.

* `-pl` followed by positive integer specifies the number of phenotypes to select for from the [target file](#markdown-header-target-file). This value defaults to 1.

* `-bl` followed by a non-negative less than that given with the `-sl` argument specifies how many phenotype signals should have their initial values perturbed. This value defaults to 0.

* `-il` followed by a non-negative specifies the number of "internal" signals that genes can target which are not directly visible to selection. This value is not constrained by any other settings. This value defaults to 4.

## Examples
Some examples I used to generate the data for my dissertation. All of them use the java flag `-Xmx35g` to ensure that each of the runs used up to (and no more than) 35g of memory since these were run on a compute cluster across heterogeneous nodes. Less memory can be used, but as population sizes are reasonably large (5000 individuals) where each individual may have tens or hundreds of genes, chromosomes of tens of thousands of characters, plus tracking various statistics over the course of their individual lifetimes, the garbage collector struggles to keep up if too little memory is allowed to be allocated unless population sizes (or genome sizes) are reduced.

In all examples given (and in all simulations run for my dissertation), the following options are identical:

* `-gf seed0.gen` - start from the same initial genome

* `-si signal0.1_1` - use the same environment file

* `-ni 10001` - run for 10,000 new generations (10,001 iterations since the initial generation is considered to be generation 0)

* `-bs 5000` - 5,000 individuals per simulation

* `-pk 0.4` - 40% of the population is replaced each generation

* `-ks 1` - the strength of selection for choosing who dies is set to 1 (refer to section 1.4.6 in my dissertation for details)

* `-sc -10` - the strength of selection for choosing who reproduces is set to -10 (refer to section 1.4.6 in my dissertation for details)

* `-pl 3` - there are 3 "phenotype" signals under selection

* `-sl 1` - there is 1 environmental signal that organisms can "sense"

* `-il 32` - there are 3 "internal" signals that genes can regulate and sense

The remaining options used may vary. Refer to [command line options](#markdown-header-command-line-options) for details. The following examples show each of the targets used as well as each type of environment option. Note, again, that they manually specify using a max heap size of 35g; if you don't have that much memory available, copy/pasting any of these could lead to system instability as memory gets filled. Adjust this value to something sane for your particular hardware. At a population size of 5000 individuals with the supplied genome file, the simulation will still run with as little as 2g of memory supplied, however this will cause the JVM garbage collector to be moderately active nearly constantly. We recommend supplying at least 8g of memory to avoid the garbage collector using significant resources. Additionally, make sure to specify an appropriate number of threads using the `-nt` option. If this is not supplied, the number of threads used for a simulation will be set to 4 (unless you are queuing up simulations using Slurm, in which case the simulation will automatically detect the number of CPU cores assigned to the task and set the number of threads equal to that).

An example using the `t3` target with a cyclical environment, per base mutation rate of `1e-4` and recombination rate of `1e-3`: 

`java -Xmx35g SimStart -do ~/staging/t3/cycle/4_3/ -gf seed0.gen -si signal0.1_1 -ni 10001 -bs 5000 -pk 0.4 -ks 1 -pl 3 -sl 1 -il 32 -tf t3 -mr 1e-4 -rr 1e-3 -ce -sc -10`

An example using the `t4` target with a constant (static) environment, and per base mutation and recombination rates of `1e-3`:

`java -Xmx35g SimStart -do ~/staging/t4/constant/3_3/ -gf seed0.gen -si signal0.1_1 -ni 10001 -bs 5000 -pk 0.4 -ks 1 -pl 3 -sl 1 -il 32 -tf t4 -mr 1e-3 -rr 1e-3 -sc -10`

An example using the `hm` target with a randomly changing environment, per base mutation rate of `1e-3` and recombination rate of `1e-4`:

`java -Xmx35g SimStart -do ~/staging/hm/rand/3_4/ -gf seed0.gen -si signal0.1_1 -ni 10001 -bs 5000 -pk 0.4 -ks 1 -pl 3 -sl 1 -il 32 -tf testScript -mr 1e-3 -rr 1e-4 -re 1 1 -sc -10`

An example using the `sin` target with a randomly changing environment and phenotypic perturbations (i.e. selection for homeostasis), and per base mutation and recombination rates of `1e-5`:

`java -Xmx35g SimStart -do ~/staging/sin/rand_hom/5_5/ -gf seed0.gen -si signal0.1_1 -ni 10001 -bs 5000 -pk 0.4 -ks 1 -pl 3 -sl 1 -il 32 -tf test.sin -mr 1e-5 -rr 1e-5 -re 1 4 -bl 3 -sc -10`