import java.util.*;
import java.math.*;
import java.io.*;
import java.util.stream.*;
import java.nio.ByteBuffer;

final class Chromosome implements Comparable<Chromosome>{
    byte[][] chrom;
    byte[][] pchrom;
    int len;
    static boolean verbose = false;
    static boolean recMeth = true;
    // number of bytes a gene uses * 4 to give total number of bases per gene
    // (2 bits per base = 4 bases per byte)
    static int geneLength = Gene.geneLength * 4; 
    private int[] lens;
    private int[] plens;
    public SortedSet<Integer> geneLocs;
    SortedSet<Integer> pgeneLocs;
    public SortedSet<Integer> trimmedLocs = new TreeSet<>();
    SortedSet<Integer> ptrimmedLocs = new TreeSet<>();
    private double sceRate;
    private boolean dead;
    private int s = 0;

    Chromosome(String[] s, double scr){
        makeChromB(s);
        init(scr);
    }

    Chromosome(Chromosome p, double scr){
        chrom = new byte[][]{new byte[p.chrom[0].length], new byte[p.chrom[1].length]};
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < chrom[i].length; j++){
            chrom[i][j] = p.chrom[i][j];
            }
        }
        init(scr);
    }
    
    Chromosome(StringBuilder[] s, double scr){
        this(new String[]{s[0].toString(), s[1].toString()}, scr);
    }
    
    Chromosome(String s, double scr){
        this(splitChr(s), scr);
    }

    @Override
    public int compareTo(Chromosome other){
        if(this.lens[0] != other.lens[0])
            return this.lens[0] - other.lens[0];
        if(this.lens[1] != other.lens[1])
            return this.lens[1] - other.lens[1];
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < this.chrom[0].length; j++){
            if(this.chrom[i][j] != other.chrom[i][j])
                return this.chrom[i][j] - other.chrom[i][j];
            }
        }
        return 0;
    }
    
    private void init(double scr){
        lens = new int[]{chrom[0].length*4, chrom[1].length*4};
        len = lens[0] + lens[1];
        geneLocs = findGenes();
        trimLocs(geneLocs, trimmedLocs);
        sceRate = scr;
        dead = lens[0] == 0 || lens[1] == 0;
        recMeth = SimStart.recMeth;
    }
    
    private void makeChromB(CharSequence[] s){
        chrom = new byte[][]{new byte[(s[0].length()+3)/4], new byte[(s[1].length()+3)/4]};
        for(int c = 0; c < chrom.length; c++){
            for(int i = 0; i < s[c].length(); i++){
                char b = s[c].charAt(i);
                switch (b){
                case '1':
                    chrom[c][i/4] += 1 << 2 * (3 - i%4);
                    break;
                case '2':
                    chrom[c][i/4] += 2 << 2 * (3 - i%4);
                    break;
                case '3':
                    chrom[c][i/4] += 3 << 2 * (3 - i%4);
                    break;
                default:
                    break;
                }
            }
        }
    }
    

    void reinit(double scr, SortedSet<Integer> gLocs, SortedSet<Integer> tLocs){
        len = lens[0] + lens[1];
        geneLocs = gLocs;
        trimmedLocs = tLocs;
        sceRate = scr;
        dead = lens[0] == 0 || lens[1] == 0;
    }


    public TreeSet<Integer> getGeneLocs(){
        return new TreeSet<Integer>(geneLocs);
    }
    

    public StringBuilder[] stringBCopy(){
        StringBuilder[] result = new StringBuilder[2];
        result[0] = new StringBuilder(baString(chrom[0]));
        result[1] = new StringBuilder(baString(chrom[1]));
        return result;
    }

    
    public String toString(){
        return baString(chrom[0]) + '@' + baString(chrom[1]);
    }

    public void lock(){
        pchrom = new byte[chrom.length][chrom[0].length];
        for(int i = 0; i < chrom.length; i++){
            pchrom[i] = chrom[i].clone();
        }
        pgeneLocs = new TreeSet<>(geneLocs);
        ptrimmedLocs = new TreeSet<>(trimmedLocs);
        plens = lens.clone();
    }

    public static String baString(byte[] b){
        StringBuilder result = new StringBuilder();
        for(byte s : b){
            String t = "000" + Integer.toUnsignedString(s, 4);
            result.append(t.substring(t.length()-4));
        }
        return result.toString();
    }

    public static String baString(byte b){
        String t = "000" + Integer.toUnsignedString(b, 4);
        return t.substring(t.length()-4);
    }
    

    public boolean dead(){
        return dead;
    }

    public int[] getLens(){
        return new int[]{lens[0], lens[1]};
    }
    
    private static String[] splitChr(String in){
        String[] result = new String[2];
        if(in.indexOf('@') > -1){
            result = in.split("@");
        }
        else{
            result[0] = in.substring(0, in.length()/2);
            result[1] = in.substring(in.length()/2);
        }
        if(result.length == 1)
            result = new String[]{result[0], ""};
        else if(result.length == 0)
            result = new String[]{"", ""};
        return result;
    }
    
    boolean pointMutate(Random rand, byte[][] chrom, TreeSet<Integer> geneLocs){
        int loc = rand.nextInt(lens[0]+lens[1]);
        int which = (loc < lens[0])? 0 : 1;
        loc = loc - lens[0];
        //System.err.println("mut\t" + loc);
        for(int i = -3; i <= 3; i++){
            // since this always results in a base change, we know that any gene start site
            // hit will be disrupted, regardless of what the base is changed to
            if(geneLocs.remove(loc + i)){
                //nodeRemoved++;
                if(verbose)
                    System.err.println("\tremoved " + (loc + i));
            }
        }

        byte toWhich = (byte)(1+rand.nextInt(3));
        int off = (which == 0)? lens[0] : 0;
        toWhich <<= 2*(3-((loc+off)%4));
        chrom[which][(loc+off)/4] ^= toWhich;

        for(int i = 0; i < 7; i++){
            int newGeneLoc = checkLocalLoc(loc+off+4*i, chrom, which);
            if(newGeneLoc >= 0){
                geneLocs.add(newGeneLoc - off);
                if(verbose)
                    System.err.println("\tadded " + (newGeneLoc - off));
            }
        }
        return true;
    }


    // checks a recently modified location to see if there is a gene start site within
    // 3 bases up/down, and returns the location if it exists within the specified
    // chromosome arm, or -1 otherwise
    private static int checkLocalLoc(int loc, byte[][] chrom, int which){
        int len = chrom[which].length*4;
        int front = Math.max(0, loc-3);
        int end = Math.min(front + 7, len-geneLength);
        if(end - front < 4){ // Can't have a start site with less than 4 bases!
            return -1;
        }

        /* Some reasoning for the following: The region of interest may cover 1, 2, or 3 bytes 
         * depending on size and offset. What I am doing is assuming that 3 bytes will be used, 
         * and then using the masks to mask part of or entire bytes which will not be used.
         */
        int rightlen = (2 * (4 - end%4));
        //              Set required number of bits to 1  Shift right to mask end bytes
        int wholemask = -1 << rightlen;
        //           byte left of loc if it exists              byte which has loc       byte right of loc
        int region = ByteBuffer.wrap(new byte[]{0, ((loc/4 > 0)? chrom[which][loc/4-1]:0),
                            chrom[which][loc/4], chrom[which][loc/4+1]}).getInt();
        
        // mask the region and then shift it right so we're only left with bits that matter
        int maskedRegion = (region & wholemask) >> rightlen;
        //System.err.println(Integer.toUnsignedString(maskedRegion, 4));
        int found = -1;
        boolean one = false;
        for(int i = end-front; i >= 4;){
            int zeroCount = Integer.numberOfTrailingZeros(maskedRegion);
            // if there are no trailing zeroes, but the 2nd bit is 0, then the rightmost base maskedRegion
            // represents is 1, and we want to see if enough zeroes preceed it to correspond to a gene start site
            one = zeroCount == 0 && (maskedRegion & 2) == 0;
            maskedRegion >>= 2;
            zeroCount = Integer.numberOfTrailingZeros(maskedRegion);
            if(one && zeroCount >= 6){
                found = i - 4;
                break;
            }
            i -= 1;
        }

        if(found >= 0){
            return found+front;
        }
        else
            return -2;
    }


    // This is a monstrosity which should probably be broken up
    public void recInit(Chromosome[] g, int wc, Random rand, double sceRate, double ptRate){
        byte[][] c1 = g[wc].pchrom;
        byte[][] c2 = g[wc+1].pchrom;
        int[] pl1 = g[wc].plens;
        int[] pl2 = g[wc+1].plens;

        SortedSet<Integer> gl1 = g[wc].pgeneLocs;
        SortedSet<Integer> gl2 = g[wc+1].pgeneLocs;
        SortedSet<Integer> tl1 = g[wc].ptrimmedLocs;
        SortedSet<Integer> tl2 = g[wc+1].ptrimmedLocs;
        int numBreaks = -1;
        float shorter1 = Math.max(Math.min(pl1[0]-1, pl2[0]-1), 0);
        float shorter2 = Math.max(Math.min(pl1[1]-1, pl2[1]-1), 0);
        //sceRate = 0;
        if(recMeth)
            numBreaks = poisSamp(sceRate, rand);
        else{
            numBreaks = poisSamp(sceRate*(shorter1+shorter2), rand);
        }
        // co for chosen one
        int co = rand.nextInt(2);

        lens[0] = 0;
        lens[1] = 0;
        if(numBreaks < 1){
            if(co == 0){
            chrom[0] = new byte[c1[0].length];
            System.arraycopy(c1[0], 0, chrom[0], 0, c1[0].length);
            chrom[1] = new byte[c1[1].length];
            System.arraycopy(c1[1], 0, chrom[1], 0, c1[1].length);
            lens[0] = pl1[0];
            lens[1] = pl1[1];
            int num = poisSamp(ptRate * (lens[0] + lens[1]), rand);
            TreeSet<Integer> ngl = new TreeSet<>(gl1);
            TreeSet<Integer> ntl = new TreeSet<>(tl1);
            for(int i = 0; i < num; i++){
                pointMutate(rand, chrom, ngl);
            }
            trimLocs(ngl, ntl);
            reinit(sceRate, ngl, ntl);
            }
            else{
            chrom[0] = new byte[c2[0].length];
            System.arraycopy(c2[0], 0, chrom[0], 0, c2[0].length);
            chrom[1] = new byte[c2[1].length];
            System.arraycopy(c2[1], 0, chrom[1], 0, c2[1].length);
            lens[0] = pl2[0];
            lens[1] = pl2[1];
            int num = poisSamp(ptRate * (lens[0] + lens[1]), rand);
            TreeSet<Integer> ngl = new TreeSet<>(gl2);
            TreeSet<Integer> ntl = new TreeSet<>(tl2);
            for(int i = 0; i < num; i++){
                pointMutate(rand, chrom, ngl);
            }
            trimLocs(ngl, ntl);
            reinit(sceRate, ngl, ntl);
            }
            return;
        }   
        int[] subBreaks = new int[2];
        // evenly distributing the number of breaks across arms
        subBreaks[0] = Math.round(numBreaks * (shorter1/(shorter1+shorter2)));
        subBreaks[1] = Math.round(numBreaks * (shorter2/(shorter1+shorter2)));
        //int j;

        TreeSet<Integer> ngl = new TreeSet<>();
        TreeSet<Integer> ntl = new TreeSet<>();
        boolean match;
        boolean flip = false;
        // making breaks and attempting to cross
        for(int w = 0; w < 2; w++){
            byte[][] lc;
            byte[][] sc;
            SortedSet<Integer> sgl;
            SortedSet<Integer> lgl;
            int lrgLen = -1;
            int smlLen = -1;

            // ArrayLists are faster to make than TreeSets, and since we need to modify
            // the values of gene locations in the first arm of each chromosome by
            // subtracting the length of that arm and we don't actually know the length
            // until it has already been built and new gene locations filled out, it is
            // faster to just allocate two container objects and use the first to fill in the 2nd
            // with the modified values than to add/remove repeatedly on a TreeSet
            ArrayList<Integer> tgl = new ArrayList<Integer>();

            if(pl1[w] > pl2[w]){
                lc = c1;
                sc = c2;
                lrgLen = pl1[w];
                smlLen = pl2[w];
                lgl = new TreeSet<>(gl1);
                lgl.addAll(tl1);
                sgl = new TreeSet<>(gl2);
                sgl.addAll(tl2);
            }
            else{
                lc = c2;
                sc = c1;
                lrgLen = pl2[w];
                smlLen = pl1[w];
                lgl = new TreeSet<>(gl2);
                lgl.addAll(tl2);
                sgl = new TreeSet<>(gl1);
                sgl.addAll(tl1);
            }
            int lOff = (w == 0)? -lrgLen : 0;
            int sOff = (w == 0)? -smlLen : 0;

            
            ArrayList<int[]> matchIndex = new ArrayList<>();
            //int zeroCount = 0;
            TreeSet<Integer> lookLocs = new TreeSet<>();
            int maxShift = geneLength*2;
            //maxShift = geneLength*10;
            int minByteHom = 6;
            int recRange = (smlLen-maxShift)/4-minByteHom;
            while(lookLocs.size() < Math.min(subBreaks[w], recRange)){
                try{
                    int nextCand = rand.nextInt(recRange);
                    lookLocs.add(nextCand*4);
                }
                catch(Exception e){
                    System.err.println(recRange);
                    throw(e);
                }
            }
            int prevL = -1;
            int prevS = -1;
            // we're using bitwise difference between two bytes as a way of measuring sequence
            // similarity at a recombination breakpoint. This is the maximum number of bits out of 8
            // that may differ in a successful crossing over
            long maxBitDiff = Math.round(0.2*minByteHom*8);
            for(int lookLoc : lookLocs){
                for(int lookOff = 0; lookOff < Math.min(maxShift, smlLen-lookLoc); lookOff += 4){
                    /*
                    if(lookOff < smlLen - 1 &&
                       lookLoc + lookOff > prevL &&
                       Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc+lookOff)/4]&0xFF)) <= maxBitDiff){
                    if(Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc+lookOff)/4]&0xFF)) != minHomBitCount(sc[w],lc[w],lookLoc,lookOff,minByteHom)){
                        System.err.println("problem");
                        System.err.println(Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc+lookOff)/4]&0xFF)) +"\t"+ (minHomBitCount(sc[w],lc[w],lookLoc,lookOff,1)));
                    }
                    matchIndex.add(new int[]{lookLoc, lookOff});
                    prevL = lookLoc + lookOff;
                    break;

                    }
                    */

                    if(lookOff < smlLen - 1 &&
                       lookLoc + lookOff > prevL &&
                       minHomBitCount(sc[w],lc[w],lookLoc,lookOff,minByteHom)<= maxBitDiff){
                        matchIndex.add(new int[]{lookLoc, lookOff});
                        prevL = lookLoc + lookOff;
                        break;
                    }

                    /*
                    else if(lookOff == 0){
                    System.err.println("up");
                    System.err.println(w + "\t" + lookLoc);
                    System.err.println((sc[w][lookLoc/4]&255) + "\t" + (lc[w][lookLoc/4]&255));
                    System.err.println((sc[w][lookLoc/4]&255)^(lc[w][lookLoc/4]&255));
                    SimStart.shutDownEverything();
                    System.exit(1);
                    }*/
                    /*
                    if(lookLoc - lookOff > prevL &&
                       Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc-lookOff)/4]&0xFF)) <= maxBitDiff){
                    matchIndex.add(new int[]{lookLoc, -lookOff});
                    prevL = lookLoc - lookOff;
                    break;
                    }
                    */
                    if(lookLoc - lookOff > prevL &&
                       minHomBitCount(sc[w],lc[w],lookLoc,-lookOff,minByteHom)<=maxBitDiff){
                        matchIndex.add(new int[]{lookLoc, -lookOff});
                        prevL = lookLoc - lookOff;
                        break;
                    }
                    /*
                    else if(lookOff == 0){
                    System.err.println("down");
                    System.err.println(w + "\t" + lookLoc);
                    System.err.println((sc[w][lookLoc/4]&255) + "\t" + (lc[w][lookLoc/4]&255));
                    System.err.println((sc[w][lookLoc/4]&255)^(lc[w][lookLoc/4]&255));
                    SimStart.shutDownEverything();
                    System.exit(1);
                    }*/
                }
            }

            if(matchIndex.size() < lookLocs.size()){
                trimLocs(ngl, ntl);
                reinit(sceRate,ngl, ntl);
                return;
            }

            int smlStart = 0;
            int lrgStart = 0;
            ArrayList<Integer> check = new ArrayList<Integer>();
            int len = 0;

            boolean pflip = flip;
            for(int[] b : matchIndex){
                if(flip){
                    if(co == 0)
                    len += b[0] - smlStart;
                    else
                    len += b[0]+b[1] - lrgStart;
                    smlStart = b[0];
                    lrgStart = b[0]+b[1];
                    flip = false;
                }
                else{
                    if(co == 0)
                    len += b[0]+b[1] - lrgStart;
                    else
                    len += b[0] - smlStart;
                    smlStart = b[0];
                    lrgStart = b[0]+b[1];
                    flip = true;
                }
            }

            if(flip){
                if(co == 0)
                    len += smlLen - smlStart;
                else
                    len += lrgLen - lrgStart;       
            }
            else{
                if(co == 0)
                    len += lrgLen - lrgStart;
                else
                    len += smlLen - smlStart;
            }
            chrom[w] = new byte[len/4];
            
            flip = pflip;
            smlStart = 0;
            lrgStart = 0;
            len = 0;
            for(int[] b : matchIndex){
                if(verbose)
                    System.out.println("b\t" + (b[0] +lOff) + "\t" + b[1]);
                if(flip){
                    // adding/subtracting by 3 so that we won't add any gene start
                    // sites which span the recombination breakpoint. We'll be searching
                    // these sites later to see if any gene start sites span these points
                    if(co == 0){
                        cheatLocs(sgl, smlStart, sOff, b[0], len, tgl, check);
                        crossOver(sc[w], smlStart/4, chrom[w], len/4, (b[0]-smlStart)/4);
                        len += b[0] - smlStart;
                    }
                    else{
                        cheatLocs(lgl, lrgStart, lOff, b[0]+b[1], len, tgl, check);
                        crossOver(lc[w], lrgStart/4, chrom[w], len/4, ((b[0]+b[1])-lrgStart)/4);
                        len += b[0]+b[1] - lrgStart;
                    }
                    smlStart = b[0];
                    lrgStart = b[0]+b[1];
                    flip = false;
                }
                else{
                    if(co == 0){
                        cheatLocs(lgl, lrgStart, lOff, b[0]+b[1], len, tgl, check);
                        crossOver(lc[w], lrgStart/4, chrom[w], len/4, ((b[0]+b[1])-lrgStart)/4);
                        len += b[0]+b[1] - lrgStart;
                    }
                    else{
                        cheatLocs(sgl, smlStart, sOff, b[0], len, tgl, check);
                        crossOver(sc[w], smlStart/4, chrom[w], len/4, (b[0]-smlStart)/4);
                        len += b[0] - smlStart;
                    }
                    smlStart = b[0];
                    lrgStart = b[0]+b[1];
                    flip = true;
                }
            }
            if(flip){
                if(co == 0){
                    cheatLocs(sgl, smlStart, sOff, smlLen, len, tgl, check);
                    crossOver(sc[w], smlStart/4, chrom[w], len/4, (smlLen-smlStart)/4);
                    len += smlLen - smlStart;
                }
                else{
                    cheatLocs(lgl, lrgStart, lOff, lrgLen, len, tgl, check);
                    crossOver(lc[w], lrgStart/4, chrom[w], len/4, (lrgLen-lrgStart)/4);
                    len += lrgLen - lrgStart;
                }
            }
            else{
                if(co == 0){
                    cheatLocs(lgl, lrgStart, lOff, lrgLen, len, tgl, check);
                    crossOver(lc[w], lrgStart/4, chrom[w], len/4, (lrgLen-lrgStart)/4);
                    len += lrgLen - lrgStart;
                }
                else{
                    cheatLocs(sgl, smlStart, sOff, smlLen, len, tgl, check);
                    crossOver(sc[w], smlStart/4, chrom[w], len/4, (smlLen-smlStart)/4);
                    len += smlLen - smlStart;
                }
            }
            lens[w] = len;
            int mod = (w == 0)? lens[0] : 0;
            for(int gl : tgl){
                if(gl > lens[w] - geneLength)
                    continue;
                ngl.add(gl - mod);
            }
            
            for(int i = 0; i < check.size(); i++){
                int gl = checkLocalLoc(check.get(i), chrom,  w);
                if(gl >= 0){
                    ngl.add(gl - mod);
                }
            }
        }

        int num = poisSamp(ptRate * (lens[0] + lens[1]), rand);
        for(int i = 0; i < num; i++){
            pointMutate(rand, chrom, ngl);
        }
        trimLocs(ngl, ntl);
        reinit(sceRate, ngl, ntl);
    }    

    private static boolean checkEquality(byte[] s1, byte[] s2){
        if(s1.length != s2.length)
            return false;
        for(int i = 0; i < s1.length; i++){
            if(s1[i] != s2[i])
            return false;
        }
        return true;
    }


    // Removes genes whose start sites exist in another gene
    private static void trimLocs(SortedSet<Integer> geneLocs, SortedSet<Integer> trimmedLocs){
        if(geneLocs.size() == 0){
            return;
        }
        geneLocs.addAll(trimmedLocs);
        trimmedLocs.clear();
        int prev = geneLocs.first()-geneLength;
        ArrayList<Integer> remove = new ArrayList<>();
        for(int l : geneLocs){
            if(l - prev < geneLength){
                //System.err.println("Removing " + l + " because of " + prev);
                remove.add(l);
            }
            else{
                prev = l;
            }
        }
        for(int r : remove){
            geneLocs.remove(r);
            trimmedLocs.add(r);
        }
        //System.err.println("~~~~~");
    }

    private int minHomBitCount(byte[] sc, byte[] lc, int lookLoc, int lookOff, int minHom){
        int result = 0;
        for(int homOff = 0; homOff < minHom; homOff++){
            result += Integer.bitCount((sc[lookLoc/4+homOff]&0xFF)^(lc[(lookLoc+lookOff)/4+homOff]&0xFF));
        }
        return result;
    }
    
    private void cheatLocs(SortedSet<Integer> pl, int cstart, int coff, int breakpoint, int len, ArrayList<Integer> tgl, ArrayList<Integer> check){
        if(cstart + 1 <= breakpoint - 2){
            for(int gl : pl.subSet(cstart+coff+1, breakpoint+coff-3)){
                int subloc = gl - (cstart + coff);
                tgl.add(len+subloc);
            }
        }
        for(int c = 0; c < 7; c++){
            check.add(len + breakpoint -cstart + 4*c);
        }
    }
    
    private static void crossOver(byte[] other, int ostart, byte[] chrom, int cstart, int len){ 
        try{
            System.arraycopy(other, ostart, chrom, cstart, len);
        }
        catch(Exception e){
            System.err.println(cstart);
            System.err.println(ostart);
            System.err.println(len);
            System.err.println(chrom.length);
            System.err.println(other.length);
            System.err.println(cstart + len);
            e.printStackTrace();
            SimStart.shutDownEverything();      
            System.exit(1);
        }
    }
    
    // given mean u and one time interval,
    // samples a point from the corresponding
    // poisson distribution. (Knuth's implementation)
    private static int poisSamp(double u, Random rand){
        double eu = Math.pow(Math.E, -u);
        double p = 1;
        int result = 0;
        do{
            result++;
            p *= rand.nextDouble();
        }while(p > eu);
        
        return result-1;
    }

    // Finds the locations in a chromosome that encode a gene start site
    //
    // Should only need to be run when an entirely new genome is created.
    // Reproduction uses parental gene locations to create offspring gene locations
    // by just checking locations around where mutation/recombination happen.
    public TreeSet<Integer> findGenes(){
        //TreeSet<Integer> bm = new TreeSet<Integer>();
        TreeSet<Integer> gl = new TreeSet<Integer>();
        for(int j = 0; j < 2; j++){
            int p = -1;
            int off = (j == 0)? -lens[0] : 0;
            int zeroCount = 0;      
            for(int i = 0; i < chrom[j].length-geneLength/4; i++){
            // exactly this byte yay!
            if(chrom[j][i] == 1){
                gl.add(i*4+off);
                zeroCount = 0;          
                continue;
            }

            // not eactly this one :( if the end of the previous one could start a
            // gene location let's see if the start of this one can
            if(zeroCount > 0){
                // how many zero bits do we have leading this byte (-24 since this is an integer
                // method)
                int leadingZeros = Integer.numberOfLeadingZeros(chrom[j][i])-24;
                // if the total number of zeros is 3 or more and they end in a 1,
                // we found a start site!
                if(leadingZeros/2 + zeroCount >= 3 && leadingZeros%2 == 1){
                int nl = leadingZeros/2 + 1 + (i-1)*4 + off;
                gl.add(nl);
                /*
                String t1 = "000" + Integer.toUnsignedString(chrom[j][i-1], 4);
                String t2 = "000" + Integer.toUnsignedString(chrom[j][i], 4);
                System.err.println(t1 + "\t" + t2);
                */
                }
            }       
            zeroCount = Integer.numberOfTrailingZeros(chrom[j][i])/2;
            }
        }
        return gl;
    }


    byte[] getBGene(int loc){
        byte[] result = new byte[geneLength/4];
        int w = -1;
        if(loc < 0){
            loc = loc + lens[0];
            w = 0;
        }
        else{
            w = 1;
        }
        int shift = 2 * (loc%4);
        int mask = ((1<<shift)-1) << (8-shift);
        for(int i = 0; i < geneLength/4; i++){
            if(shift > 0){
                try{
                    result[i] = (byte)(chrom[w][loc/4+i] << shift);
                    result[i] ^= (byte)((chrom[w][loc/4+i+1]&mask) >>> (8-shift));
                }
                catch(ArrayIndexOutOfBoundsException e){
                    System.err.println();
                    System.err.println("Loc: " + loc);
                    System.err.println("Length (bytes): " + chrom[w].length + " (bases) " + chrom[w].length*4);
                    e.printStackTrace();
                    SimStart.shutDownEverything();
                    System.exit(1);
                }
            }
            else{
                result[i] = (chrom[w][loc/4+i]);
            }
        }
        return result;
    }
}
