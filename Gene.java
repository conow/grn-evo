import java.util.*;
import java.math.*;
import java.util.concurrent.*;
import java.nio.ByteBuffer;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;

// NOTE: byte&255 treats a (signed) byte as unsigned and converts it to an always positive integer

class Gene{
    // first byte is start "codon", and is not used for determining gene function
    static final int geneLength = 13;
    final double regulate;
    final double baseExpr;
    final int[] bindingSite;
    final int target;
    final int spec;
    final double maxExpr;
    final int direction;
    final double baseReg;
    int[] targets;
    int[] loc = new int[]{-1, -1};
    double totTarg = 0;
    double output;
    boolean used = false;
    //double regulated = 0;
    double artificialReg = 0;
    static int signalLen = 2;
    static int peturbLen = 0;
    static int phenoLen = 1;
    static int internalLen = 4;
    static double regDiv = Math.pow(2,32)-1;
    final int type;
    static Comparator<Gene> unique = new Comparator<Gene>(){
        public int compare(Gene first, Gene second){
            if(first.target == second.target)
                if(Arrays.hashCode(first.bindingSite) == Arrays.hashCode(second.bindingSite))
                    return (int)Math.signum(first.regulate) - (int)Math.signum(second.regulate);
                else
                    return Integer.compare(Arrays.hashCode(first.bindingSite), Arrays.hashCode(second.bindingSite));
                return Integer.compare(first.target, second.target);
        }
    };

    static Comparator<Gene> each = new Comparator<Gene>(){
        public int compare(Gene first, Gene second){
            int ucomp = Gene.unique.compare(first, second);
            if(ucomp != 0)
                return ucomp;
            else
                return (first.loc[0] != second.loc[0])? Integer.compare(first.loc[0], second.loc[0]) : Integer.compare(first.loc[1], second.loc[1]);
        }
    };

    public String toString(){
        return String.format("%d_%d_%f", Arrays.toString(bindingSite), target, regulate);
    }

    public String tabString(){
        return String.format("%8s,%8s\t%8d,%8s\t%f\t%f\t%f\t%f\t[%d, %d]", Arrays.toString(bindingSite), Integer.toString(bindingSite[0], (bindingSite[0] >= 0)? 4 : 10), target, Integer.toString(target, (target >= 0)? 4 : 10), regulate, output, maxExpr, baseExpr, loc[0], loc[1]);
    }

    public void spike(double amt){
        artificialReg = amt;
    }

    public static int setSignalLen(int newLen){
        signalLen = newLen;
        return signalLen;
    }

    public static int setPeturbLen(int newLen){
        peturbLen = newLen;
        return peturbLen;
    }

    public static int setPhenoLen(int newLen){
        phenoLen = newLen;
        return phenoLen;
    }

    public static int setInternalLen(int newLen){
        internalLen = newLen;
        return internalLen;
    }

    Gene(byte[] code, int wc, int l){
        if(code.length != geneLength){
            System.err.println("Incorrect gene length: " + (code.length * 4));
            System.err.println("Chrom: " + wc + ", loc: " + l);
            SimStart.shutDownEverything();
            System.exit(1);
        }

        bindingSite = new int[2];

        loc[wc] = l;

        int cc = 1;
        // I resisted having this for a while, but alas, it is the most straightforward way to
        // accomplish having a clear distinction between different types of genes
        type = (code[cc++]&255) % 5;

        // if this is a signal sensitive gene, (type 0), then it's activation site must fall in the
        // range of [-256, -1]
        //ByteBuffer bind = ByteBuffer.wrap(new byte[]{0, 0, code[2], code[3]});
        //int tempSite = bind.getInt();
        int tempSite = code[cc++]&255;
        int tempSite2 = code[cc++]&255;
        if(type == 0 || type == 2){
            /*
            bindingSite[0] = -1 * (tempSite%signalLen) - 1;
            bindingSite[1] = -1 * (tempSite2%signalLen) - 1;
            */
            bindingSite[0] = -1 * (tempSite%(phenoLen+signalLen)) - 1;
            bindingSite[1] = -1 * (tempSite2%(phenoLen+signalLen)) - 1;
        }
        else if(type == 1 || type == 3){// otherwise it's over the whole thing
            bindingSite[0] = tempSite%internalLen;//Integer.bitCount(tempSite);
            bindingSite[1] = tempSite2%internalLen;
        }
        else{ 
            // Type 4 sets up initial conditions for the GRN. They shouldn't have a binding site
            // so we set this value to something that doesn't bind to anything. We'll use
            // tempSite as part of determining what the target of this gene is
            bindingSite[0] = Integer.MAX_VALUE;
            bindingSite[1] = Integer.MAX_VALUE;
        }
        // [0, 32767] correspond to targetting other genes
        // [-256, -1] correspond to producing phenotype visible to selection (after the same 
        // calculation as for binding sites)
        // For gene-gene interactions, targets are allowed to influence binding sites which are within
        // 1 basepair mutation from the encoded target
        //ByteBuffer targ = ByteBuffer.wrap(new byte[]{0, 0, code[3], code[4]});
        //int tempTar = targ.getInt();
        int tempTar = (code[cc++]^code[cc++])&255;
        //int tempTar = code[3]&255;
        //spec = ByteBuffer.wrap(new byte[]{0, 0, code[5], code[6]}).getInt()%internalLen;
        spec = ((code[cc++]^code[cc++])&255)%internalLen;
        if(type == 1 || type == 2)
            target = -1 * (tempTar%phenoLen) - signalLen - 1;
        else if(type == 0 || type == 3)
            target = tempTar%internalLen;//Integer.bitCount(tempTar);
        else{
            // Type 4 targets can be any of the internal targets or phenotype targets
            // as setting initial values for the GRN to work with
            target = (tempTar+tempSite)%(internalLen+phenoLen);
        }

        genTargets(target, spec);

        // Determine the direction of selection.
        // Bitcount < 4 means negative
        // >= 4 and < 6 means zero
        // <= 6 means positive
        // the motivation being that switching from positive to negative
        // should require moving through zero, rather than allowing a single
        // mutation to flip between positive and negative
        int directBC = Integer.bitCount(code[cc++]&255);
        direction = (directBC < 3)? -1 : (directBC < 6)? 0 : 1;
        // Determine the regulation level [-128, 127]
        //long reg = ByteBuffer.wrap(new byte[]{0,0,0,0,code[9],code[10],code[11],code[8]}).getLong();
        //baseReg = Math.log(reg)/regDiv;

        baseReg = 0.7*((code[cc++]&255)/255.0+(code[cc++]&255)/255.0+(code[cc++]&255)/255.0+(code[cc++]&255)/255.0)/4.0;
        //baseReg = ByteBuffer.wrap(new byte[]{0,0,0,0,code[cc++],code[cc++],code[cc++],code[cc++]}).getLong()/regDiv;

        regulate = baseReg * direction;
        //regulate = code[5]/64.+code[6]/64.+code[7]/64.+code[8]/64.;
        /*
        if(regulate < -5)
            System.err.println(regulate);
        */
        //regulate = code[6]/12.8;//32.;
        // Base expression level [-128, 127]
        // anything non-positive will be functionally zero
        //ByteBuffer be = ByteBuffer.wrap(new byte[]{code[10], code[11], code[12], code[13]});
        //baseExpr = be.getInt()/(Integer.MAX_VALUE + 0.);
        //baseExpr = 10*(code[10])/128.0*(code[11]&255)/255.0*(code[12]&255)/255.0*(code[13]&255)/255.0;
        //baseExpr = (be.getInt()-(1<<15))/3276.3;
        //baseExpr = code[10]/128.0+code[11]/128.0+code[12]/128.0+code[13]/128.0+code[14]/128.0;
        if(type == 4){
            baseExpr = regulate;
        }
        else
            baseExpr = 0;

        //maxExpr = (code[15]&255)/255.*4;
        maxExpr = 0;
    }

    private void genTargets(int target, int spec){
        if(type == 1 || type == 2)
            targets = new int[]{target};
        else if(type == 4){
            if(target < internalLen)
                targets = new int[]{target};
            else
                targets = new int[]{target - (internalLen + phenoLen + signalLen + 1)};
        }
        else{
            /*
            targets = new int[]{target,
                    target^(1<<0), target^(1<<2), target^(1<<4), target^(1<<6),
                    target^(1<<8), target^(1<<10), target^(1<<12), target^(1<<14),
                    target^(2<<0), target^(2<<2), target^(2<<4), target^(2<<6),
                    target^(2<<8), target^(2<<10), target^(2<<12), target^(2<<14),
                    target^(3<<0), target^(3<<2), target^(3<<4), target^(3<<6),
                    target^(3<<8), target^(3<<10), target^(3<<12), target^(3<<14)};
                    /**/
            TreeSet<Integer> tempTarget = new TreeSet<Integer>();
            tempTarget.add(target);
            for(int i = 0; i < 7; i += 2){
                tempTarget.add(target^((1<<i)&spec));
                tempTarget.add(target^((2<<i)&spec));
                tempTarget.add(target^((3<<i)&spec));
            }
            targets = new int[tempTarget.size()];
            for(int i = 0; i < targets.length; i++){
                targets[i] = tempTarget.pollFirst();
            }
            /*
            targets = new int[]{target,//};/*
                    target^((1<<0)&spec), target^((1<<2)&spec), target^((1<<4)&spec), target^((1<<6)&spec),//};/*,
                    target^((2<<0)&spec), target^((2<<2)&spec), target^((2<<4)&spec), target^((2<<6)&spec),//};/*,
                    target^((3<<0)&spec), target^((3<<2)&spec), target^((3<<4)&spec), target^((3<<6)&spec)};//*/
                    /**/
            //int temp = Integer.bitCount(target);
            //targets = new int[]{temp};
        }
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> rMap = new HashMap<String, Object>();
        rMap.put("Locs", loc);
        rMap.put("Type", type);
        rMap.put("BindingSite", bindingSite);
        rMap.put("Target", target);
        rMap.put("Specificity", spec);
        rMap.put("Targets", targets);
        rMap.put("BaseReg", baseReg);
        rMap.put("Direction", direction);
        rMap.put("Regulation", regulate);
        rMap.put("BaseExpr", baseExpr);
        //rMap.put("MaxExpr", maxExpr);
        rMap.put("Used", used);
        //rMap.put("Regulated", regulated);
        //rMap.put("ArtificialReg", artificialReg);
        //rMap.put("Output", output);
        rMap.put("TotTarg", totTarg);
        return rMap;
    }
}
