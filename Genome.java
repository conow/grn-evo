import java.util.*;
import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.util.zip.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

final class Genome implements Comparable<Genome>{
    static boolean verbose = false;
    
    private Random r;
    final Chromosome[] gen;
    static ObjectMapper m = new ObjectMapper();
    Map<Integer, Integer> bindingSiteCounts = new HashMap<>();
    double ptRate;
    double rcRate;
    int numC;
    private List<Gene> geneList = new ArrayList<Gene>();


    Genome(Genome g, Random r, double ptRate, double rcRate){
        this.r = r;
        gen = new Chromosome[g.gen.length];
        for(int i = 0; i < gen.length; i++){
            gen[i] = new Chromosome(g.gen[i], rcRate);
        }
        this.numC = g.numC;
        this.ptRate = ptRate;
        this.rcRate = rcRate;
    }


    Genome(Path fname, Random rand, double ptRate, double sceRate) throws IOException{
        this.ptRate = ptRate;
        this.rcRate = sceRate;
        r = rand;
        int tempC = 0;
        BufferedReader in;
        if(fname.toString().endsWith("gz")){
            JsonNode pop = Environment.loadPop(fname);
            in = new BufferedReader(new StringReader(Environment.getFittestGenome(pop)));
        }
        else{
            in = new BufferedReader(new FileReader(fname.toString()));
        }
        ArrayList<Chromosome> tempGen = new ArrayList<Chromosome>();
        String s = "";
        while((s = in.readLine()) != null){
            tempGen.add(new Chromosome(s, rcRate));
            tempC += (tempGen.get(tempGen.size() - 1).dead())? 0 : 1;
        }
        numC = tempC;
        gen = new Chromosome[tempGen.size()];
        tempGen.toArray(gen);
    }


    Genome(String genS, Random rand, double ptRate, double sceRate){
        this.ptRate = ptRate;
        this.rcRate = sceRate;
        r = rand;
        String[] genSS = genS.trim().split("\n");
        gen = new Chromosome[genSS.length];
        int tempC = 0;
        for(int i = 0; i < genSS.length; i++){
            gen[i] = new Chromosome(genSS[i], rcRate);
            tempC += (gen[i].dead())? 0 : 1;
        }
        numC = tempC;
    }

    // This is for network analysis purposes only
    Genome(String genS){
        ptRate = -1;
        rcRate = -1;
        String[] genSS = genS.trim().split("\n");
        gen = new Chromosome[genSS.length];
        r = new Random();
        int tempC = 0;
        for(int i = 0; i < genSS.length; i++){
            gen[i] = new Chromosome(genSS[i], rcRate);
            tempC += (gen[i].dead())? 0 : 1;
        }
        numC = tempC;
    }


    static Genome[] makePopGenomes(Path fname, Random rand, double ptRate, double rcRate){
        BufferedReader in;
        JsonNode pop = Environment.loadPop(fname);
        int popSize = pop.get("Size").asInt();
        Genome[] result = new Genome[popSize];
        if(!SimStart.bsSet)
            SimStart.size = popSize;
        for(int i = 0; i < popSize; i++){
            String gen = pop.get("["+i+"]").get("Genome").asText();
            result[i] = new Genome(gen, new Random(rand.nextLong()), ptRate, rcRate);
        }
        return result;
    }


    @Override
    public int compareTo(Genome other){
        for(int i = 0; i < this.gen.length; i++){
            int test = this.gen[i].compareTo(other.gen[i]);
            if(test != 0)
            return test;
        }
        return 0;
    }
    

    public void initialize(Genome g1, Genome g2, Random rand, double ptRate, double rcRate){
        r = rand;
        int tempC = 0;
        for(int i = 0; i < gen.length; i+=2){
            if(verbose)
            System.err.println("g1");
            gen[i].recInit(g1.gen, i, r, rcRate, ptRate);
            if(verbose)
            System.err.println("g2");
            gen[i+1].recInit(g2.gen, i, r, rcRate, ptRate);
            tempC += (gen[i].dead())? 0 : 1;
            tempC += (gen[i+1].dead())? 0 : 1;
        }
        numC = tempC;
        this.ptRate = ptRate;
        this.rcRate = rcRate;
    }


    public boolean dead(){
        boolean result = false;
        for(Chromosome c : gen){
            result = result || c.dead();

        }       
        return result;
    }


    public void lock(){
        for(Chromosome c : gen){
            c.lock();
        }
    }


    public int size(){
        int result = 0;
        for(Chromosome c : gen){
            result += c.len;
        }
        return result;
    }


    public List<Gene> makeGenes(){
        geneList.clear();
        bindingSiteCounts.clear();
        int lc = 0;
        for(Chromosome c : gen){
            for(int i : c.geneLocs){
                Gene g = new Gene(c.getBGene(i), lc, i);
                geneList.add(g);
                bindingSiteCounts.put(g.bindingSite[0], bindingSiteCounts.getOrDefault(g.bindingSite[0],0)+1);
                if(g.bindingSite.length == 2)
                    bindingSiteCounts.put(g.bindingSite[1], bindingSiteCounts.getOrDefault(g.bindingSite[1],0)+1);
            }
            lc++;
        }
        for(Gene g : geneList){
            double totTarg = 0.;
            for(int t : g.targets){
                totTarg += bindingSiteCounts.getOrDefault(t, 0);
            }
            g.totTarg = totTarg;
        }
        return geneList;
    }


    public String toString(){
        String result = "";
        for(int i = 0; i < gen.length; i++){
            result += gen[i].toString() + "\n";
        }
        return result;
    }
}   

