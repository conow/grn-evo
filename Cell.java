import java.util.*;
import java.math.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.stream.*;

// there used to be more possible states
// could just replace this with a boolean
enum State {
    DEAD, VIABLE;
}

class Cell implements Callable<Cell>{
    static Comparator<Cell> comp = new Comparator<Cell>(){
        public int compare(Cell first, Cell second){
        return ((Integer)first.loc).compareTo(second.loc);
        }
    };

    // compare by number of offspring produced in an organism's lifetime
    static Comparator<Cell> byFit = new Comparator<Cell>(){
        public int compare(Cell first, Cell second){
        if(first.numBabby.get() == second.numBabby.get()){
            if(first.fitness == second.fitness){
                return comp.compare(first, second);
            }
            return ((Double)second.fitness).compareTo(first.fitness);
        }
        return ((Integer)first.numBabby.get()).compareTo(second.numBabby.get());
        }
    };
    
    static Comparator<Cell> byGenome = new Comparator<Cell>(){
        public int compare(Cell first, Cell second){
            return first.gen.compareTo(second.gen);
        }
    };
    int loc;
    static int maxAge = 500;
    static double failThresh = 1;
    static boolean verbose = false;
    Genome gen;
    Genome pgen;
    List<Gene> genes;
    NavigableMap<Double, Cell> breedTree;
    Integer iternum;
    Map<Integer, Double> regulation = new HashMap<>();
    Map<Integer, Double> externalReg =  new HashMap<>();
    Map<Integer, Double> nextRegulation = new HashMap<>();
    Map<Integer, Double> savedRegulation = new HashMap<>();
    MatrixGrn grn = new MatrixGrn();
    SortedSet<Gene> uniqueGenes = new TreeSet<>(Gene.unique);
    SortedSet<Gene> uniqueInternal = new TreeSet<>(Gene.unique);
    SortedSet<Gene> usedGenes = new TreeSet<>(Gene.each);
    int lived; // age. dont know why i didnt call it age...
    // since we're now having GRN updates multiple times per
    // environmental update, it's probably useful to keep track
    // of how many iterations the GRN has been updated in addition to
    // how many global updates the individual has survived.
    int iters;
    int generation;
    int genesUsed;
    int[] pLocs;
    int[] pIterBorn;
    int born;
    AtomicInteger numBabby = new AtomicInteger();
    AtomicInteger numAttempt = new AtomicInteger();
    AtomicBoolean mated = new AtomicBoolean(false);
    boolean killed = false;

    Environment env;
    double ptRate;
    double rcRate;
    static boolean mutableRates = false;
    boolean doa;
    int numC;
    int bestAge = 0;
    double fitness = 0;
    double normFit = 0;
    double ps = 0;
    double prevFitness = 0;
    double prevNormFit = 0;
    double bestFitness = 0;
    int numGene;
    ArrayList<Double> fitDist = new ArrayList<>();
    ArrayList<Double> normFitDist = new ArrayList<>();
    ArrayList<double[]> sigTraj = new ArrayList<>();
    ArrayList<double[]> pairTraj = new ArrayList<>();
    ArrayList<Double> interiorDist = new ArrayList<>();
    ArrayList<double[][]> tarDist = new ArrayList<>();
    ArrayList<double[][]> phenDist = new ArrayList<>();
    ArrayList<Integer> offDist = new ArrayList<>();
    ArrayList<Integer> attDist = new ArrayList<>();
    Random rand;

    State state = State.DEAD;
    
    Cell(Cell old, Random r){
        loc = old.loc;
        rand = r;
    }

    
    private Cell(int loc, Random r){
        this.loc = loc;
        rand = r;
    }

    // clone constructor
    Cell(Cell original, int loc){
        this(loc, original.rand, original.gen, original.ptRate, original.rcRate, original.breedTree, original.env, original.iternum);
        this.regulation = new HashMap<>(original.regulation);
    }

    private Cell(int loc, Random r, Genome[] gen, double ptRate, double rcRate, NavigableMap<Double, Cell> bt, Environment env, Integer i){
        this.loc = loc;
        rand = r;
        this.ptRate = ptRate;
        this.rcRate = rcRate;   
        this.env = env;
        breedTree = bt;
        iternum = i;
        initialize(gen);
        if(gen != null)
            this.gen.lock();
    }

    Cell(int loc, Random r, Genome gen, double ptRate, double rcRate, NavigableMap<Double, Cell> bt, Environment env,  Integer i){
        this(loc, r, new Genome[]{gen}, ptRate, rcRate, bt, env, i);
    }

    Cell(int loc, Random r, Genome gen, NavigableMap<Double, Cell> bt, Environment env, Integer i){
        this(loc, r, gen, -1, -1, bt, env, i);
    }
    
    Cell(int loc, Random r, Cell old, NavigableMap<Double, Cell> bt, Environment env, Integer i){
        this(loc, r, new Genome[]{old.gen}, old.ptRate, old.rcRate, bt, env, i);
        pLocs[0] = old.loc;
        pIterBorn[0] = old.born;
        generation = old.generation + 1;
    }

    Cell(int loc, Random r, Cell old1, Cell old2, NavigableMap<Double, Cell> bt, Environment env, Integer i){
        this(loc, r, new Genome[]{old1.gen, old2.gen}, old1.ptRate, old1.rcRate, bt, env, i);
        generation = Math.max(old1.generation, old2.generation);
        pLocs[0] = old1.loc;
        pLocs[1] = old2.loc;
        pIterBorn[0] = old1.born;
        pIterBorn[1] = old2.born;
    }

    public static Cell dummyCell(){
        int loc = -1;
        Random r = new Random();
        Genome[] gen = null;
        double ptRate = -1;
        double rcRate = -1;
        TreeMap<Double, Cell> bt = new TreeMap<>();
        double[] s = new double[1];
        double[] p = new double[1];
        Integer i = -1;
        Environment env = null;
        return new Cell(loc, r, gen, ptRate, rcRate, bt, env, i);
    }
    
    public double getFitness(){
        return fitness;
    }

    private double rateMod(){
        double result = 1/Math.exp(rand.nextDouble()*709);
        if(rand.nextDouble() < 0.5)
            result = -1 * result;
        return result;
    }

    private boolean initialize(Genome[] gen){
        if(gen == null)
            return false;
        //numBabby = new AtomicInteger();
        //numAttempt = new AtomicInteger();
        killed = false;
        fitness = 0;
        prevFitness = 0;
        prevNormFit = 0;
        fitDist.clear();
        normFitDist.clear();
        tarDist.clear();
        phenDist.clear();
        sigTraj.clear();
        pairTraj.clear();
        interiorDist.clear();
        offDist.clear();
        attDist.clear();
        /*
        if(mutableRates){
            ptRate = 0;
            //rcRate = 0;
            for(Genome g : gen){
            ptRate += g.ptRate;
            //rcRate += g.rcRate;
            }
            ptRate /= (double)gen.length;
            //rcRate /= (double)gen.length;
            ptRate = Math.max(0, Math.min(1, ptRate+rateMod()));
            //rcRate = Math.max(0, Math.min(1, rcRate+rateMod()));
        }
        /**/
        /**/

        /**/
        if(gen.length == 1)
            this.gen = new Genome(gen[0], rand, ptRate, rcRate);
        else{
            this.gen.initialize(gen[0], gen[1], rand, ptRate, rcRate);
        }
        doa = this.gen.dead();
        numC = this.gen.numC;
        lived = 0;
        iters = 0;
        if(doa){
            lived = -1;
            return false;
        }
        genes = this.gen.makeGenes();
        grn.init(genes);
        numGene = genes.size();
        pLocs = new int[2];
        pLocs[0] = -1;
        pLocs[1] = -1;
        pIterBorn = new int[2];
        pIterBorn[0] = -1;
        pIterBorn[1] = -1;
        born = iternum.intValue();
        regulation.clear();
        //categorizeCell();
        usedGenes.clear();
        uniqueGenes.clear();
        uniqueInternal.clear();
        fitness = 0;
        fitness = Math.max(fitness, 0.);
        prevFitness = fitness;
        return true;
    }

    public void categorizeCell(){
        if(doa || lived > maxAge ||
           (lived > 1 && numAttempt.get() > 0 && numBabby.get()/(double)numAttempt.get() < failThresh)){
            //System.err.println(lived);
            state = State.DEAD;
        }
        else{
            state = State.VIABLE;
        }
    }

    // This is not meant to be used to produce an adjacency matrix to represent the
    // grn for use in analysis. grn is not used during the simulation itself.
    //
    // Note that this creates "nodes" in the resulting network which correspond to input
    // and output signals as well as the genes themselves
    public static double[][] makeGrn(MatrixGrn grn){
        return grn.getDoubleArray();
    }

    public String grnString(){
        return grn.toString();
        /*
        StringBuilder result = new StringBuilder();
        double[][] grn = makeGrn(this.grn);
        for(double[] row : grn){
            result.append(Arrays.stream(row).mapToObj(w -> w + "").collect(Collectors.joining("\t")));
            result.append("\n");
        }
        //System.err.println(result.toString());
        return result.toString();
        */
    }

    public String geneList(){
        StringBuilder result = new StringBuilder();
        result.append("bs10\tbs4\tt10\tt4\treg\tregd\tout\tme\tbe\tloc\n");
        for(Gene g : genes){
            result.append(g.tabString() + "\n");
        }
        return result.toString();
    }

    
    public Map<String, Object> dumpMap(){
        TreeMap<String, Object> dumpMap = new TreeMap<>();
        dumpMap.put("Genome", this.gen.toString());
        dumpMap.put("Age", lived);
        dumpMap.put("Genes", Arrays.stream(genes.toArray(new Gene[genes.size()])).map(i->i.toMap()).toArray());
        dumpMap.put("Generation", generation);
        dumpMap.put("Offspring", numBabby.get());
        dumpMap.put("NumMating", numAttempt.get());
        dumpMap.put("Fitness", fitness);
        dumpMap.put("SigTraj", sigTraj.toArray());
        dumpMap.put("PairTraj", pairTraj.toArray());
        dumpMap.put("FitTraj", fitDist.toArray());
        dumpMap.put("NormFitTraj", normFitDist.toArray());
        dumpMap.put("TarTraj", tarDist.toArray());
        dumpMap.put("PhenTraj", phenDist.toArray());
        dumpMap.put("OffTraj", offDist.toArray());
        dumpMap.put("AttTraj", attDist.toArray());
        dumpMap.put("Grn", grn.grn.getData());
        dumpMap.put("InitialVals", grn.values);
        dumpMap.put("MutRate", ptRate);
        dumpMap.put("RecRate", rcRate);
        return dumpMap;
    }

    // given the index of a gene in genes, change its expression level by
    // a specified amount
    public void spikeGene(int num, double amt){
        genes.get(num).spike(amt);
    }

    public void spikeReg(int num, double amt){
        externalReg.put(num, amt);
    }
    
    // edit starting from here
    private boolean reproduce(){
        killed = false;
        if(breedTree.size() == 0){
            return false;
        }
        boolean verbose = true;
        ArrayList<Cell> candNeigh = new ArrayList<Cell>();
        int ind = 0;
        Cell p1 = getParent();
        Cell p2 = getParent();

        try{
            initialize(new Genome[]{p1.gen, p2.gen});
        }
        catch(Exception e){
            System.err.println("p1: " + p1.loc + " killed? " + p1.killed);
            System.err.println("p2: " + p2.loc + " killed? " + p2.killed);
            throw(e);
        }
        // if reproduction results in a dead on arrival offspring,
        // we just want to exit because it won't get used and is going to be
        // "re-rolled". Additionally, we only want to count the number of
        // successful offspring parental cells have, not just every mating attempt
        p1.numAttempt.getAndIncrement();
        p2.numAttempt.getAndIncrement();
        pLocs[0] = p1.loc;
        pLocs[1] = p2.loc;
        if(doa){
            return false;
        }
        p1.mated.compareAndSet(false, true);
        p2.mated.compareAndSet(false, true);
        p1.numBabby.getAndIncrement();
        p2.numBabby.getAndIncrement();
        generation = Math.max(p1.generation, p2.generation) + 1;
        pIterBorn[0] = p1.born;
        pIterBorn[1] = p2.born;

        if(mutableRates){
            //ptRate = SimStart.ptRate+2*Math.pow(10, -(Math.max(3, 1/((p1.prevNormFit+p2.prevNormFit)))));
            ptRate = SimStart.ptRate+1*Math.pow(10, Math.min(-3, -2+Math.log10((p1.prevNormFit+p2.prevNormFit)/2.0)));
            //rcRate = SimStart.rcRate+2*Math.pow(10, -(Math.max(3, -1+1/((p1.prevNormFit+p2.prevNormFit)))));
            rcRate = SimStart.rcRate+1*Math.pow(10, Math.min(-3, -2+Math.log10((p1.prevNormFit+p2.prevNormFit)/2.0)));
        }

        Set<Integer> regTargets = new TreeSet<>(p1.savedRegulation.keySet());
        regTargets.addAll(p2.savedRegulation.keySet());
        for(int i : regTargets){
            regulation.put(i, (p1.savedRegulation.getOrDefault(i, 0.) + p2.savedRegulation.getOrDefault(i, 0.))/2.);
        }

        return true;
    }


    private Cell getParent(){
        // sometimes because of floating point error the last key will be slightly greater than 1.
        // It shouldn't be very significant, but to be safe we multiply by that value rather than assume
        // it will always be 1.
        double intSelect = rand.nextDouble() * breedTree.lastKey();
        Cell result;
        try{
            result = breedTree.ceilingEntry(intSelect).getValue();
            /* Since we're now making the breed tree before anything is killed,
               this is expected to happen on occasion. For what it's worth I don't
               think I've seen this print statement actually execute in years
            if(result.killed || result.doa)
            System.err.println("This is bad");
            */
        }
        catch(NullPointerException npe){
            System.err.println("*** "+intSelect);
            System.err.println(breedTree.firstKey());
            System.err.println(breedTree.lastKey());
            throw(npe);
        }
        return result;
    }


    public Cell call(){
        if(gen == null){
            System.err.println("No (or invalid) cell specified as target for selection! Shutting down...");
            SimStart.shutDownEverything();
            System.exit(1);
        }
        if(killed)
            reproduce();
        int rcount = 0;
        while(doa){
            reproduce();
            rcount++;
            if(doa && rcount > 100){
                categorizeCell();
                return this;
            }
        }
        
        lived++;

        int n = grn.n;

        /*
        double[] internalSignal = new double[n];
        System.arraycopy(Environment.getSignalAt(i), 0, internalSignal, n - Gene.signalLen, n);
        */
        grn.makeExpressTraj(SimStart.internalIters, SimStart.internalIntStep, env.getSignalAt(loc, true));
        tarDist.add(env.evalFitness(loc, grn));//*(1+0.000000001*numGene+0.000001*gen.size());
        phenDist.add(grn.getPhenoTraj().getData());
        double exprSum = 0;
        for(double[] pa : phenDist.get(phenDist.size()-1)){
            exprSum += Arrays.stream(pa).sum();
        }
        double fitmod = Math.max(0, Math.log10(lived) - 2);// + 
        //Math.max(0, Math.log10(gen.size()/200000.)) + 
        //Math.max(0, numGene/500);
        fitness = 0;
        normFit = 0;
        double[] tarSums = new double[grn.pairDist.length];
        int count = 0;
        /*
        for(double[] tar : tarDist.get(tarDist.size()-1)){
            count = 0;
            for(double t : tar){
            try{
                tarSums[count] += t;
                count++;
            }
            catch(Exception e){
                System.err.println(tarSums.length);
                System.err.println(tarDist.get(tarDist.size()-1).length);
                throw(e);
            }
            }
        }
        */
        count = 0;
        for(double d : grn.pairDist){
            fitness += d;///tarSums[count];
            count++;
        }
        double tempSum = 0;
        for(double[] ts : tarDist.get(tarDist.size()-1)){
            for(double t : ts){
                tempSum += Math.pow(t,2);
            }
        }
        tempSum = Math.sqrt(tempSum);
        tempSum = Math.max(tempSum, 0.000001);
        if(tempSum != 0)
            normFit = fitness/tempSum;
        else
            normFit = -fitness;
        normFitDist.add(normFit);
        fitness = fitness+fitmod;
        if(fitness < 0.00001 && normFit > 0.5)
            System.err.println("is this real life?");
            
        fitDist.add(fitness);
        pairTraj.add(grn.pairDist);
        sigTraj.add(env.getSignalAt(loc, true));
        if(lived > 1){
            offDist.add(numBabby.get());        
            attDist.add(numAttempt.get());
        }
        else{
            offDist.add(0);
            attDist.add(0);
        }
        prevFitness = fitness;
        prevNormFit = normFit;
        categorizeCell();
        return this;
    }

    private static double lastNfitAvg(List<Double> fitDist){
        double result = 0;
        int n = Math.min(fitDist.size(), 6);
        for(int i = fitDist.size() - n; i < fitDist.size(); i++){
            result += fitDist.get(i);
        }
        return result/(double)n;
    }   
}
